# -*- coding: utf-8 -*-
"""@file Menu.py
@brief ME 305 lab assignments and homework assignments
@mainpage
      


                

@section sec_intro    Introduction
                      Links to the repository of various source codes and Lab documentation
                      
@section sec_links    Links to coursework
         <B>Source Code Repository: </B><br>   https://bitbucket.org/zachrann/me305_labs/src/master/ <br>
         <br>
         <B>Lab 0x00 Documentation:  </B><br>  https://zachrann.bitbucket.io/Lab0/ <br>
         <br>
         <B>Lab 0x01 Documentation:  </B><br>  https://zachrann.bitbucket.io/Lab1/ <br>
         <br>
         <B>Lab 0x02 Documentation: </B><br>   https://zachrann.bitbucket.io/Lab2/ <br>
         <br>
         <B>Lab 0x03 Documentation:  </B><br>  https://zachrann.bitbucket.io/Lab3/ <br>
         <br>
         <B>Lab 0x04 Documentation: </B><br>   https://zachrann.bitbucket.io/Lab4/ <br>
         <br>
         <B>Lab 0x05 Documentation:  </B><br>  https://zachrann.bitbucket.io/Lab5/ <br>
         <br>
         <B>Lab 0x0FF (Term Project) Documentation::</B><br> https://zachrann.bitbucket.io/LabFF/ <br>
         <br>
         <B>Homework 2:</B><br>
         @ref hw_2_page <br>
         <br>
         <B>Homework 3:</B><br>
         @ref hw_3_page
         
@author  Zachary Rannalli
<br>
<B>Homework 2:</B><br>
@ref hw_2_page <br>
<br>
<B>Homework 3:</B><br>
@ref hw_3_page
"""

