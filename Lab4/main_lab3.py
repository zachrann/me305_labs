z"""@file        main_lab3.py
   @brief       FSM main program
   @details        File imports hardware and user tasks and sets the period at which they update. Additionally, a motor driver
                is created.
   @author         Zach Rannalli
   @author         Clayton Elwell
   @date          November 18, 2021
"""


#import task_encoderLAB3
import motor_driver
#import task_motor
import task_userLAB3
import shares
import task_hardware

## Defines the period [ms] (and thus frequency) at which the task_encoder is run. In this case it is currently set at 2 ms.

## Defines the period [ms] (and thus frequency) at which the task_user is run. In this case it is currently set at 10 ms.
T_user = 40
## Defines the period [ms] (and thus frequency) at which the task_user is run. In this case it is currently set at 10 ms.
T_motor = 2

#Index references for shared motor array
## Encoder number
E_N = 0
## Encoder Position
POS = 1
## Reference position
REF_POS = 2
## Encoder Velocity
VEL = 3     
## Reference velocity
REF_VEL= 4

## Assigned Duty Cycle
DUTY = 5
## Encoder zeroing Boolean Value
ZERO = 6
## Checks for motor fault trigger
CHECK_FAULT = 7
## Disables the motor fault
FIX_FAULT = 8
## Kp gain value
KP = 9
    
if __name__ == '__main__':   
    ''' @brief    Creates objects for the encoder tasks and user tasks, and subsequently updates them at their respectively chosen frequencies.
        
    '''
    ## Motor 1 shared data
    ShareM1 = shares.ShareMotor([1, 0, 0, 0, 0, 0, False, False, False, 2])
    ## Motor 2 shared data
    ShareM2 = shares.ShareMotor([2, 0, 0, 0, 0, 0, False, False, False, 2])
    ## Instantiation of a motor driver object
    motor_drv = motor_driver.DRV8847(3)
    
    
    ## Attribute associated with the hardware 1 object
    motorTask1 = task_hardware.Task_Hardware(T_motor, ShareM1, motor_drv)
    ## Attribute associated with the hardware 2 object
    motorTask2 = task_hardware.Task_Hardware(T_motor, ShareM2, motor_drv)
    ## Attribute associated with the user task
    userTask = task_userLAB3.TaskUser(T_user, ShareM1, ShareM2)
    
    while(True):
        
        try: 
            
           # encoderTask1.run()
          #  encoderTask2.run()
            
            userTask.run()
            
            motorTask1.run()
            motorTask2.run()
            
            
            
        except KeyboardInterrupt:
            break
    motor_drv.disable()
    print('Program Terminating')
    ## The task object associated with the LED blinking code
    



   
 

