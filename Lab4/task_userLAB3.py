"""@file task_userLAB3.py
   @brief    Module that creates the puTTY user interface.
   @details  This module reads the user's input from the keyboard and subsequently inteprets the user input to determine
               whether to zero the encoder, display the encoder position or encoder delta, or collect encoder position data and
               display that data on the puTTY virtual command prompt.
   @author  Zachary Rannalli
   @author  Clayton Elwell
   @date    November 18, 2021
"""

#import task_encoder
import utime
import pyb
import array as arr

## Constant associated with State 0
S0_INIT = 0

## Constant associated with State 1
S1_WAIT_FOR_INPUT = 1
## Asks the user for a Kp input
S2_REQUEST = 2
## Time delay state before the new duty cycle is sent to the motor
S3_WAIT = 3

#Index references for shared motor array
## Encoder number
EN = 0
## Encoder Position
POS = 1
## Reference position
REF_POS = 2
## Encoder Velocity
VEL = 3     
## Reference velocity
REF_VEL= 4

## Assigned Duty Cycle
DUTY = 5
## Encoder zeroing Boolean Value
ZERO = 6
## Checks for motor fault trigger
CHECK_FAULT = 7
## Disables the motor fault
FIX_FAULT = 8
## Kp gain value
KP= 9

## Attribute that allows the method to communuicate with the USB's virtual command prompt.
CommReader = pyb.USB_VCP()
## Prompt that asks for propotional gain or reference velocity
Proportional_Prompt = ["Enter Proportional Gain (%*s/rad): "]



class TaskUser:
    '''@brief   Class that creates the puTTY user interface and facilitates the user's interaction with the encoder via the task_encoder module.
       @details Class that handles all serial communication between the user and backend. Creates a user interface.
    '''
    
    
    def __init__(self, period, Share1, Share2):
        '''@brief           Method that initiates the necessary attributes for communication with the task_encoder and the various lists and timers to collect the encoder data and display it in the puTTY 
                            terminal.
           @param           period    The period at which the task_user class is executed.
           @param           Share1   Shared variable associated with the first motor
           @param           Share2   Shared variable associated with the second motor
        '''
        
        
        ## The current state of the task_user FSM.
        self.state = S0_INIT
        
        ## Number of iterations of the FSM
        self.runs = 0
        
        ## The frequency at which the task_user class is executed.
        self.period = period
                
        ## Attribute that allows the run() method to determine if the FSM should start.
        self.nexttime = utime.ticks_add(self.period, utime.ticks_ms())
        
        
        #self.UserCOMM = True
        ## Boolean value that determines if the write() module should begin collecting data from the encoder.
       # self.displayp = [False, False]
        ## Kp gain
        self.Kp = 0
        ## Time at which the Kp gain will be written based on the difference between its own value and the current time
        self.time_delay = 0
        ## Index describing Kp and the reference velocity
        self.Kp_index = 0
      
        ## 30 second value that allows the write() module to determine if data collection is done.
        self.DisplayPositionTime = 30010
        ## Time duration for the step function to occur over.
        self.DisplayStepTime = 10010
        ## Initial time value that is used with self.DisplayPositionTime to determine if data collection is done.
        self.to = 0
        ## Attribute associated with encoder and motor 1 shares
        self.Share1 = Share1
        ## Attribute associated with encoder and motor 2 shares
        self.Share2 = Share2
        
        ## Boolean for controlling if user can change the motor duty cycle
      #  self.initDuty = [False, False]
      #  self.zeroshare = [0,0]
        
        ## Variable to track time
        self.time = utime.ticks_ms()
        
        ## Boolean for controlling position display
        #self.initPos = [False, False]
        
        
        
        ## Array to collect time data from both motor/encoder combos
        #self.tArray = [[],[]]
        
        ## Array to collect position data from both encoders
        #self.pArray = [[],[]]
        
        ## Array to collect velocity data from both encoders
        #self.vArray = [[],[]]
        
        
        
    def run(self):
        '''@brief         Method that creates the user interface and FSM that interprets the user's character inputs.
           @details       Transitions through the states once per period and prints to user interfance. If the Kp values is changed
                          then a 1 second time delay occurs before setting the new motor duty cycle. State 2 is the Kp gain adjustment 
                          state. Once the user has finished their input, the FSM transitions back to State 1.
            
        '''
        current_time = utime.ticks_ms()
        
        
        
        if(utime.ticks_diff(current_time, self.nexttime) >= 0):
            self.nexttime += self.period
            if(self.state == S0_INIT):
                #Execute code for state 1
                print("\033c", end="")
                print('--------------------------------\n'
                      '     USER COMMAND INTERFACE     \n'
                      '--------------------------------\n'
                      'z: Set encoder 1 position to zero \n'
                      'Z: Set encoder 2 position to zero \n'
                      'p: Print encoder 1 position to PuTTY \n'
                      'P: Print encoder 2 position to PuTTY \n'
                      'd: Print encoder 1 delta to puTTY \n'
                      'D: Print encoder 2 delta to puTTY \n'
                      'm: Set duty cycle for motor 1\n'
                      'M: Set duty cycle for motor 2\n'
                      'c: Clear fault condition triggerd by DRV8847\n'
                      'g: Collect encoder 1 data for 30 seconds, then print to puTTY\n'
                      'G: Collect encoder G data for 30 seconds, then print to puTTY\n'
                      's: End data collection prematurely\n'
                      '1: Set proportional gain and run step function on motor 1\n'
                      '2: Set proportional gain and run step function on motor 2\n'
                      'ESC: Display user interface controls\n'
                      '--------------------------------')
        
                self.transition_to(S1_WAIT_FOR_INPUT) #Transition to state 1
            
                self.setupCollection()
            
            elif(self.state == S1_WAIT_FOR_INPUT):
        
                keyCommand = self.read()
                
                self.write(keyCommand[0],self.Share1)

                self.write(keyCommand[0] + 32, self.Share2)
             
                        
                if(self.Kp != 0):
                    
                    if utime.ticks_diff(current_time, self.time_delay) >= 1000:
                        print("Motor starting...")
                        self.MotorStepped.write(KP, self.Kp)
                        self.Kp = 0
    
            elif(self.state == S2_REQUEST):
                
                keyCommand = self.read()
                
                Kp = self.requestDuty(0, keyCommand[0], Proportional_Prompt)
                
                if keyCommand == b'\x1b'[0]:
                    self.transition_to(S0_INIT)
                elif(Kp != None):
                    
                    self.MotorStepped.write(REF_VEL, Kp)
                    self.setupTime(self.DisplayStepTime, self.MotorStepped.read(EN)-1, [DUTY, VEL], 'Duty [%], Velocity [rad/s]')
                    self.time_delay = current_time
                    #self.Kp_index = 0
                        
                    self.build[0] = ''
                       
                    self.transition_to(S1_WAIT_FOR_INPUT)
                    print('Entering State 1')
                   
                   # self.build[0] = ''
                   # self.Kp = Kp
                    #self.Kp_index += 1
                   # self.requestDuty(0, b'None'[0], Proportional_Prompt[self.Kp_index])
    
    def write(self,keyCommand,Shares):
        '''@brief              Method that interprets the user input via keyCommand and subsequently determines whether to display collected data, collect data, display the encoder delta, or
                                display the encoder position. 
           @details            Contains logic for user input recognition, such as printing position or collecting data, or executing a step function on the motor.
           @param              keyCommand ASCII value that corresponds to the character inputted by the user.
           @param              Shares Tuple value that contains the most recent encoder position and encoder delta values
           
        '''
        
       # pos = Shares.read(POS)
       # vel = Shares.read(VEL)
        en = Shares.read(EN) - 1
       # duty = Shares.read(DUTY)
        
        
        if keyCommand == b'\x1b'[0]:
            self.transition_to(S0_INIT)
        
        elif keyCommand == b'z'[0]:
            Shares.write(ZERO, True)
           # print(str(Shares.read(ZERO)))
            print('Zeroing encoder ' + str(en+1) + ' position')
            
        elif keyCommand == b'd'[0]:
            print('Motor ' +str(en+1) + ' Velocity [rad/s]:' + str(Shares.read(VEL)))
            
        elif keyCommand == b'p'[0]:
            print('Motor ' +str(en+1) + ' Position [rad]: ' + str(Shares.read(POS)))
                
        elif keyCommand == b'g'[0] and not self.displayp[en]:
            self.setupTime(self.DisplayPositionTime, en, [POS, VEL], 'Position [rad], Velocity [rad/s]')
            
            print('Collecting Motor' + str(en+1) + ' data...')
           # self.displayp[en] = True
           # self.to = utime.ticks_ms()
            
        elif keyCommand == b'm'[0]:
            
            self.initDuty[en] = True
            self.build[en] = ''
            
        elif keyCommand == b'c'[0]:
            Shares.write(FIX_FAULT, True)
            print('Motor ' +str(en+1) + ' fault disabled')
        
        elif keyCommand == b's'[0] or Shares.read(CHECK_FAULT):
            self.tf[en] = utime.ticks_ms()
        
        if(self.displayp[en] and 0 == self.nexttime%(2*self.period)//self.period):
            m1data = Shares.read(self.record[en][0])
            m2data = Shares.read(self.record[en][1])
            print('recording setup')
            self.recordData(en, m1data, m2data)
            
        if(self.initDuty[en]):
            vel = self.requestDuty(en, keyCommand, "Motor " + str(en+1) + ", Input motor speed: ")
            if(vel != None):
                #print('got to here')
                Shares.write(REF_VEL, vel)
                #print(str(Shares.read(REF_VEL)))
                
        elif(keyCommand == 49 + 33*en and not self.initDuty[not en]):
            self.transition_to(S2_REQUEST)
            ## Step function shared data
            self.MotorStepped = Shares
            print("Entering State 2")
            #kp = self.requestDuty(en, b'None'[0], Proportional_Prompt)
            #self.MotorStepped.write(KP, kp)
            #
            
            
        # if(self.displayp[en]):
        #     self.recordData(en, pos, vel, keyCommand == b's'[0])
            
        # if(self.initDuty[en]):
        #     returnDuty = self.requestDuty(en, keyCommand, duty)
        #     if(returnDuty != duty):
        #         Shares.write(DUTY, returnDuty)
           
       # if (self.displayp == True and (keyCommand == b's' or utime.ticks_diff(utime.ticks_add(self.to, self.DisplayPositionTime), current_time) <= 0)):
        #    print('Time [sec], Position [ticks]')
         #   for n in range(len(self.tlist)):
          #      print('{:}, {:}'.format(self.tlist[n]/1000,self.plist[n]))
           # self.displayp = False
            #self.tlist = []
            #self.plist = []
            
       # elif(self.displayp):
       #     self.tlist.append(utime.ticks_diff(current_time, self.to))
       #     self.plist.append(pos)
        
      
    def read(self):
        '''@brief     Method that directly reads the VCP's character input and returns that character input to the run() method, or displays the user interface again.
           @return    Character inputted by the user via puTTY.
        '''
        
        #keyCommand = b''  #defining keyCommand so error is not thrown
        
        if(CommReader.any()):
            
            keyCommand = CommReader.read(1)
            
            CommReader.read()
             
            
            return keyCommand
            
        return b' '
          
    def recordData(self, e_n, m1data, m2data):
        '''@brief               Transitions the FSM to a new state
           @param e_n   The motor/encoder ID
           @param m1data   The first variable being recorded
           @param m2data   The second variable being recorded
           
        '''
        
        
        current_time = utime.ticks_ms()
        print('recording')
        if utime.ticks_diff(self.tf[e_n], current_time) <= 0 :
            self.Share1.write(REF_VEL, 0)
            self.Share2.write(REF_VEL, 0)
            self.Share1.write(KP, 0)
            self.Share2.write(KP, 0)
            print('Motor ' + str(e_n+1) + ', Data:\n'
                  'Time [s], {:}'.format(self.prompt[e_n]))
            for n in range(len(self.tArray[e_n])):
                print('{:}, {:}, {:}'.format(self.tArray[e_n][n]/1000,self.pArray[e_n][n],self.vArray[e_n][n]))
        
       
           
            self.displayp[e_n] = False
            self.tArray[e_n] = [arr.array('f',[]),arr.array('f',[])]
            self.pArray[e_n] = [arr.array('f',[]),arr.array('f',[])]
            self.vArray[e_n] = [arr.array('f',[]),arr.array('f',[])]
           
        else:
           self.tArray[e_n].append(utime.ticks_diff(current_time, self.to))
           print(utime.ticks_diff(current_time, self.t0[e_n]))
           self.pArray[e_n].append(m1data)
           self.vArray[e_n].append(m2data)
           
    def requestDuty(self,e_n, keyCommand, request):
        '''@brief               Transitions the FSM to a new state
           @details     Handles the user input from the keyboard.
           @param      e_n Motor ID value
           @param      keyCommand ASCII value of the keyboard character read in the VCP
           @param      request Prompts user to input the Kp and reference velocity
        '''
        
     #   current_time = utime.ticks_ms()
        keyCommand = keyCommand - e_n*32
        
        
            
        
        if (keyCommand>=b'0'[0] and keyCommand<=b'9'[0]):
            new = str(keyCommand - 48)
            self.build[e_n] = self.build[e_n] + new
        
        elif keyCommand == 127:
            self.build[e_n] = self.build[e_n][:-1]            
        elif keyCommand == 46:
            if "." not in self.build[e_n]:
                self.build[e_n] = self.build[e_n] + '.'
                
        elif keyCommand == 45:
            if self.build[e_n] == '' or self.build[e_n][0] != '-':
                
                self.build[e_n] = '-' +self.build[e_n]
                
        elif keyCommand == 13:
            self.initDuty[e_n] = False
            if (self.build[e_n] == '' or self.build[e_n] == '.' or self.build[e_n] == '-'):
                self.build[e_n] = '0'
                
            hold = self.build[e_n]
            self.build[e_n] = ''
            return float(hold)
            
        if(keyCommand != 32):
            print('\033c', end='')
            print(request + str(self.build[e_n]))
            
        return None
    
    def transition_to(self, new_state):
        '''@brief               Transitions the FSM to a new state
           @param new_state     The next state in the FSM to transition to
        '''
        self.state = new_state
            

    def setupCollection(self):
        '''@brief  Creates arrays to collect data, and time arrays
        '''
         
        self.Kp_index = 0
        ## Flags to indicate if data collection should occur
        self.initDuty = [False, False]
        ## Attribute that stores user input values
        self.build = ['','']
        ## Attribute that determines if the data should be displayed
        self.displayp = [False, False]
        ## End time array
        self.tf = arr.array('l',[0,0])
        ## Start time array
        self.t0 = arr.array('l',[0,0])
        ## Attribute that stores recorded data
        self.record = [[0,0],[0,0]]
        ## Kp or reference velocity prompts
        self.prompt = ['','']
        
        
        ## Array to collect time data from both motor/encoder combos
        self.tArray = [arr.array('f',[]), arr.array('f',[])]
        
        ## Array to collect position data from both encoders
        self.pArray = [arr.array('f',[]), arr.array('f',[])]
        
        ## Array to collect velocity data from both encoders
        self.vArray = [arr.array('f',[]), arr.array('f',[])]
        
    def setupTime(self, deltat, ID, data, user):
        '''@brief  Sets up the data to record either step function response or position and velocity
           @details Setups up data to be collected based on the recording time, motor ID, and what data to be recorded
           @param deltat  Time that data should be recorded
           @param ID      Motor and encoder ID
           @param data    Data to be recorded
           @param user    Title for the data to be recorded
        '''
        
        #print('Collecting Motor ' + str(ID+1) + ' Data'.format(user))
        self.displayp[ID] = True
        
        self.t0[ID] = utime.ticks_ms()
        self.tf[ID] = utime.ticks_diff(self.t0[ID], deltat)
        self.record[ID] = data
        self.prompt[ID] = user