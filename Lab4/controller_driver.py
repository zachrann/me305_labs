# -*- coding: utf-8 -*-
"""@file       controller_driver.py
   @brief      Closed loop controller with methods to set an arbitary duty cycle.
   @details    The controller calculates the difference between a reference and measured velocity to determine the error.
               The Kp gain, time difference, and error magnitude are used to calculate and return a motor duty cycle.
   @author     Zach Rannalli
   @author     Clayton Elwell
   @date       November 18, 2021
"""

class ClosedLoop():
    '''@brief
       @details
       
    '''
    
    def __init__(self, satLimitLow, satLimitHigh, initKp):
        '''@brief   Constructs a closed loop controller
           @details Sets saturations limits and in initial Kp gain.
        '''
        ## Attribute associated with the Kp controller Gain
        self.Kp = initKp
        ## Lower saturation limit value, -100
        self.satLimitLow = satLimitLow
        ## Upper saturation limit value, 100
        self.satLimitHigh = satLimitHigh
        # Sum of the errors
        #self.error_sum = 0
        ## Previous error value
        self.last_error = 0
        
        
    def duty(self, setpoint, measure):
        '''@brief    Computes and returns the actuation value based on the measured and referenced values
           @details  Uses the setpoint and measured velocities to determine the error. The velocities are determined in the
                     hardware task.
           @param    setpoint The reference velocity
           @param    measure  The measured velocity
           @return   The duty cycle which is calculated with the run() method.
        '''
        error = setpoint - measure
        
        #self.error_sum += (self.last_error + error) * deltat * .5
        
        #error_delta = (error - self.last_error) / deltat
        
        self.last_error = error
        
        
        duty = self.Kp*error
        #print(duty)
        return self.run(duty)
        
    def run(self, duty):
        '''@brief    Calculates the new saturation limit.
           @details  Determines if the duty is too large, compared to what is calculated in the duty() method.
           @return   The new saturation limit.
        '''
                
        if duty >= self.satLimitHigh:
            
            return self.satLimitHigh
            
        elif duty <= self.satLimitLow:
            
            return self.satLimitLow
        
        else:
            return duty
        #return self.actuation    
        
        
    def get_Kp(self):
        '''@brief    Method that returns the value of the proportional motor gain
           @return   The current value of the proportional gain
        '''
        return self.Kp
        
        
    def set_Kp(self, input_Kp):
        '''@brief    Method that changes the value of the proportional motor gain
           
        '''
        self.Kp = input_Kp
        
        
        

