''' @file motor_driver.py
    @brief  Motor driver which contains the DRV8847 class, and a Motor class that controls the motor duty cycle.
    @details DRV8847 class enables and disables the motor. In addition, it checks for faults and can return a motor object 
               that is instantiated from the Motor class.
    @author   Zach Rannalli
    @author   Clayton Elwell
    @date    October 18, 2021
'''
import pyb
import time

class DRV8847:
    ''' @brief A motor driver class for the DRV8847 from TI.
        @details   Objects of this class can be used to configure the DRV8847
                     motor driver and to create one or moreobjects of the
                     Motor class which can be used to perform motor
                     control.

                     stuff for both motors at the same time

         Refer to the DRV8847 datasheet here:
         https://www.ti.com/lit/ds/symlink/drv8847.pdf
     '''
    
    def __init__(self, timChannel):
        ''' @brief Initializes and returns a DRV8847 object.
            
    

        '''
        # Timer
        #self.timer = pyb.Timer(timerID, freq = 20000)
        
         ##  Timer for IN1 and IN2 pins
        self.timX = pyb.Timer(timChannel, freq = 20000)
       
        ## H/L attribute that enables/disables motor driver
        self.SLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        
        ## H/L attribute that triggers the driver fault condition
        self.FAULT = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.OUT_PP)
          
        ## External interrupt that is used to detect faults. If they are detected the callback function fault_cb disables the motor
        self.faultTrigger = pyb.ExtInt(self.FAULT, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
        ## Attribute that signals if the motor fault has been triggered.
        self.fault = False
        

    def enable (self):
        ''' @brief Brings the DRV8847 out of sleep mode.
        
        '''
        self.faultTrigger.disable()
        self.SLEEP.high()
        self.fault = False
        time.sleep(.025)
        self.faultTrigger.enable()
        

    def disable (self):
        ''' @brief Puts the DRV8847 in sleep mode.
        '''
        self.SLEEP.low()
        

    def fault_cb (self, IRQ_src):
        ''' @brief Callback function to run on fault condition.
        @param IRQ_src The source of the interrupt request.
        '''
        print('Fault detected; disabling motor...')
        self.fault = True
        self.disable()
    

    def motor (self, mpin1, mpin2, channel1, channel2):
        ''' @brief Initializes and returns a motor object associated with the DRV8847.
        @return An object of class Motor
        '''
        
        
        return Motor(mpin1, mpin2, channel1, channel2, self.timX)



class Motor:
    ''' @brief A motor class for one channel of the DRV8847.
    @details Objects of this class can be used to apply PWM to a given
    DC motor.
    
    everything to do with one motor but not the other
    
    '''

    def __init__(self, mpin1, mpin2, channel1, channel2, timX):
        ''' @brief Initializes and returns a motor object associated with the DRV8847.
        @details Objects of this class should not be instantiated
                 that to create Motor objects using the method
                 DRV8847.motor().
 
        '''
        ## Attribute that assigns CPU pin PB4 to driver IN1 pin
        pinIN1 = pyb.Pin(mpin1, pyb.Pin.OUT_PP)
        ## Attribute that assigns CPU pin PB5 to driver IN2 pin
        pinIN2 = pyb.Pin(mpin2, pyb.Pin.OUT_PP)
        
        
        
        
        ## Assigning the pin1 parameter to the STM32L476RG channel 1
        self.t1ch1 = timX.channel(channel1, pyb.Timer.PWM, pin=pinIN1)
        ## Assigning the pin2 parameter to the STM32L476RG channel 2
        self.t1ch2 = timX.channel(channel2, pyb.Timer.PWM, pin=pinIN2)
        
        
    def set_duty (self, duty):
         ''' @brief Set the PWM duty cycle for the motor channel.
         @details This method sets the duty cycle to be sent
         to the motor to the given level. Positive values
         cause effort in one direction, negative values
         in the opposite direction.
         @param duty A signed number holding the duty
         cycle of the PWM signal sent to the motor
         '''
         if duty >= 0:
                self.t1ch1.pulse_width_percent(duty)
                self.t1ch2.pulse_width_percent(0)
         else:
                duty = abs(duty)
                self.t1ch1.pulse_width_percent(0)
                self.t1ch2.pulse_width_percent(duty)
         
         

     
     
        