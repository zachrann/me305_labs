'''@file ResultsLab4.py

@page lab_4_page Lab 4 Mini Report

REPORT BODY

Here are plots of the improved motor reponse as the proportional gain is changed:
                \image html lab4plotresized.PNG "Lab 4 Proportional Control Response"            
    <br>
    
Here is the block diagram that depicts the structure of the closed loop controller:
                \image html simulinkblock.PNG "Lab 4 Controller Block Diagram"

Here is the Task Diagram and respective FSM's for Lab 4.
                \image html lab4fsmresized.JPG "Lab 4 Task Diagram and FSMs"

'''



