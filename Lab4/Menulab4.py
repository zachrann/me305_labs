# -*- coding: utf-8 -*-
"""@file Menulab4.py
@brief Lab 0x04 Closed Loop Speed Control
@mainpage
      


                

@section sec_intro    Introduction
                      We created a closed loop controller, in order to eliminate any error in the speed of the motors.

         
@author  Zachary Rannalli

@ref lab_4_page <br>
"""

