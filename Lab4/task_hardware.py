'''@file    task_hardware.py
   @brief      Task that assigns motors and encoders to their respective hardware pins, checks for motor faults, and sets duty cycle
   @details    Sets the motors and encoders to their respective pins and channel numbers, and subsequently enables the motor driver.
            A controller object is instantiated, which will be used to control the duty based on a reference and measured velocity.
   @author     Zach Rannalli
   @author     Clayton Elwell
   @date       November 18, 2021
'''


import pyb
import utime
import controller_driver
import math
import encoder_lab3

## Assigns pin B6 to this attribute
pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
## Assigns pin B7 to this attribute
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
## Assigns pin C6 to this attribute
pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
## Assigns pin C7 to this attribute
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)


# Motor pins
## Assigns pin B4 to this attribute
pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
## Assigns pin B5 to this attribute
pinB5 = pyb.Pin(pyb.Pin.cpu.B5)
## Assigns pin B0 to this attribute
pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
## Assigns pin B1 to this attribute
pinB1 = pyb.Pin(pyb.Pin.cpu.B1)

## Encoder number
E_N = 0
## Encoder Position
POS = 1
## Reference position
REF_POS = 2
## Encoder Velocity
VEL = 3     
## Reference velocity
REF_VEL= 4

## Assigned Duty Cycle
DUTY = 5
## Encoder zeroing Boolean Value
ZERO = 6
## Chceks for motor fault trigger
CHECK_FAULT = 7
## Disables the motor fault.
FIX_FAULT = 8
## Kp gain value
KP= 9
## Value to convert from ticks to radians
tick2rad = (2*math.pi/4000)

class Task_Hardware:
    ''' 
    @brief                  Encoder and motor task methods.
        @details                Contains logic to be used with hardware based on what is desired from commands sent by
                                task user.
    '''
    
    def __init__(self, period, Share, motor_drv):
        ''' 
            @brief                  Encoder and motor task methods.
            @details                Contains logic to be used with hardware based on what is desired from commands sent by
                                task user.
            @param period  Period at which the task updates
            @param Share   Shared data values
            @param motor_drv Motor driver object
        '''
        ## The period at which the task_encoder updates the encoder position.
        self.period = period*1000
        ## An integer value representing the current state of the task_encoder FSM.
        #self.state = S0_INIT
        ## Timer that increases by an interval of the assigned task_encoder period.
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## The previous update time
        self.last_time = utime.ticks_ms()
        
        ## [E_N, POS, REF_POS, VEL, REF_VEL, DUTY, ZERO, CHECK_FAULT, FIX_FAULT, DUTY]
        self.Shares = Share
        
        ## Attribute associated with the motor driver 
        self.motor_drv = motor_drv
    
        if self.Shares.read(E_N) == 1:
            ## Attribute associated with the motor object
            self.motor = self.motor_drv.motor(pinB4, pinB5, 1, 2)
            ## Attribute associated with the encoder object
            self.encoder = encoder_lab3.Encoder(pinB6, pinB7, 4)
            
        elif self.Shares.read(E_N) == 2:
            #print('second motor setup')
            self.motor = self.motor_drv.motor(pinB0, pinB1, 3, 4)
            self.encoder = encoder_lab3.Encoder(pinC6, pinC7, 8)
            
        self.motor_drv.enable()
        ## Instantiation of a controller object
        self.Controller = controller_driver.ClosedLoop(-100, 100, self.Shares.read(KP))   
        
    
    def run(self):
        ''' 
            @brief       Encoder and motor task methods.
            @details     Contains logic to be used with hardware based on what is desired from commands sent by
                         task user.
        '''
       # print('running at all')
        if (utime.ticks_diff(utime.ticks_ms(), self.next_time) >= 0):
            #print('updating')
            self.update()
            
            if (self.Shares.read(ZERO)):
                self.encoder.set_position(0)
                self.Shares.write(ZERO, False)
                
            if (self.motor_drv.fault):
                self.Shares.write(CHECK_FAULT, True)
                
            if self.Shares.read(FIX_FAULT):
                self.Shares.write(REF_VEL, 0)
                self.Shares.write(KP, 0)
                self.motor_drv.enable()
                self.Shares.write(CHECK_FAULT, False)
                self.Shares.write(FIX_FAULT, False)
                
            if (self.Shares.read(KP) != self.Controller.get_Kp):
                self.Controller.set_Kp(self.Shares.read(KP))
    
    def update(self):
        ''' @brief                  Encoder and motor task methods.
        @details                Contains logic to be used with hardware based on what is desired from commands sent by
                                task user.
        '''
        
        # Encoder update stuff
        
        tdelta = utime.ticks_diff(utime.ticks_us(), self.last_time)
        self.encoder.update()
        self.next_time += self.period
        self.last_time = utime.ticks_us()
        #print('position and velocity are set')
        self.Shares.write(POS,self.encoder.get_position()*tick2rad)
        self.Shares.write(VEL,self.encoder.get_delta()*tick2rad/ ((self.period + tdelta)/1000000))
        #print('ugh')
       # print(str(self.Shares.read(POS)))
        # Motor update stuff
        #print('setting duty cycle')
        duty = self.Controller.duty(self.Shares.read(REF_VEL), self.Shares.read(VEL))
        self.Shares.write(DUTY, duty)
        #print(str(self.Shares.read(DUTY)))
        self.motor.set_duty(duty)
    
    