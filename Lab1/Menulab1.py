# -*- coding: utf-8 -*-
"""@file Menulab1.py
@brief Lab 0x01 Getting Started with Hardware
@mainpage
      


                

@section sec_intro    Introduction
                      In this lab we were introduced the the hardware for the first time, and were tasked with making an LED blink with different inputs.
                      
@section sec_links    Links for lab 1
         Lab 0x01 Finite State Machine: https://imgur.com/a/sqMIrPM <br>
         Lab 0x01 LED Demonstration: https://www.youtube.com/watch?v=4aoz-tYsuVk&ab_channel=ClaytonE
         
@author  Zachary Rannalli

"""

