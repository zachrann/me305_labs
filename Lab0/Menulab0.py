# -*- coding: utf-8 -*-
"""@file Menulab0.py
@brief ME 305 lab 0x00 Fibonacci Sequence
@mainpage
      


                

@section sec_intro    Introduction
                      In this lab we created a program where the user could retrieve any reasonable index of the Fibonacci Sequence in far under a second.
@author    Zach Rannalli
"""

