# -*- coding: utf-8 -*-
"""@file fib_function.py
@brief Program that allows user to retrieve any index of the Fibonacci Sequence

@author: Zach Rannalli
@date Septemper 23, 2021
"""

def fib(idx):
    '''
    @brief This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired
    Fibonacci number
    '''
    ##Creates starting matrix
    F = [0,1]
    if idx == 0:
        return 0
    elif idx == 1:
        return 1
    elif idx > 1:
        ##Index counter
        B = 1
        while B < idx:
            F.append(F[B] + F[B-1])
            B += 1
        return F[idx]
        
    

if __name__ == '__main__':
    
    while True:
        idx = input('Please specify an index: ')

        try:
            idx=int(idx)
        except ValueError:
            print ('Index must be an integer')
            continue
        if idx < 0:
            print ('Index must be positive')
            continue
        print ('Fibonacci number at '
           'index {:} is {:}.'.format(idx,fib(idx)))
        end = input('To quit type q or to continue hit any other key: ')
        if end == 'q':
            break
        else:
            continue
    
