# -*- coding: utf-8 -*-
'''@file ResultsHW2.py

@page hw_2_page Homework 2 B&P Simulation Hand Calculations

Here are the hand calculations to derive the equations of motion for the ball and plate platform simulation.
            
Kinematics
    \image html hw2p1.PNG "Ball and plate kinematics part 1"
More Kinematics
    \image html hw2p1.5.PNG "Ball and plate velocity and acceleration vectors"
    <br>
Kinetics
    \image html hw2p2.PNG "Ball and plate kinetics part 1"
More Kinetics    
    \image html hw2p3.PNG "Ball and plate kinetics part 2; ball kinetics"
<br>
Equations of Motion
    \image html hw2p4.PNG "Equations of motion"     
    
'''
