"""@file task_user.py
   @brief    Module that creates the puTTY user interface.
   @details  This module reads the user's input from the keyboard and subsequently inteprets the user input to determine
            whether to zero the encoder, display the encoder position or encoder delta, or collect encoder position data and
            display that data on the puTTY virtual command prompt.
   @author        Zachary Rannalli
   @author        Clayton Elwell
"""

#import task_encoder
import utime
import pyb

## Constant associated with State 0
S0_INIT = 0

## Constant associated with State 1
S1_WAIT_FOR_INPUT = 1


class Task_User:
    '''@brief   Class that creates the puTTY user interface and facilitates the user's interaction with the encoder via the task_encoder module.
    
    '''
    
    
    def __init__(self, period):
        '''@brief           Method that initiates the necessary attributes for communication with the task_encoder and the various lists and timers to collect the encoder data and display it in the puTTY 
                            terminal.
           @param period    The period at which the task_user class is executed.
        '''
        ## Attribute that allows the method to communuicate with the USB's virtual command prompt.
        self.CommReader = pyb.USB_VCP()
        
        ## The current state of the task_user FSM.
        self.state = S0_INIT
        
        ## Number of iterations of the FSM
        self.runs = 0
        
        ## The frequency at which the task_user class is executed.
        self.period = period
                
        ## Attribute that allows the run() method to determine if the FSM should start.
        self.nexttime = utime.ticks_add(self.period, utime.ticks_ms())
        
        
        #self.UserCOMM = True
        ## Boolean value that determines if the write() module should begin collecting data from the encoder.
        self.displayp = False
        
       # self.noPrint = utime.ticks_ms()
        ## List that holds the time values associated with each update of the encoder position.
        self.tlist = []
        ## List that holds the position values associated with each update of the encoder.
        self.plist = []
        ## 30 second value that allows the write() module to determine if data collection is done.
        self.DisplayPositionTime = 30010
        ## Initial time value that is used with self.DisplayPositionTime to determine if data collection is done.
        self.to = 0
        
        #self.keyCommand = b'x'
        
    def run(self, update):
        '''@brief         Method that creates the user interface and FSM that interprets the user's character inputs.
           @param update  Tuple with encoder position and encoder delta values.
           @return        True/False value for "zero" attribute in main. 
        '''
        if(utime.ticks_diff(utime.ticks_ms(), self.nexttime) >= 0):
            self.nexttime += self.period
            if(self.state == S0_INIT):
                #Execute code for state 1
                print('--------------------------------\n'
                      '     USER COMMAND INTERFACE     \n'
                      '--------------------------------\n'
                      'z: Set encoder position to zero \n'
                      'p: Print encoder position to PuTTY \n'
                      'd: Print encoder delta to puTTY \n'
                      'g: Collect encoder data for 30 seconds, then print to puTTY\n'
                      's: End data collection prematurely\n'
                      'ESC: Display user interface controls\n'
                      '--------------------------------')
        
                self.transition_to(S1_WAIT_FOR_INPUT) #Transition to state 1
            
            elif(self.state == S1_WAIT_FOR_INPUT):
        
                keyCommand = self.read()
                zero = self.write(keyCommand,update)
                return zero
        
        return False
    
    
    def write(self,keyCommand,update):
        '''@brief              Method that interprets the user input via keyCommand and subsequently determines whether to display collected data, collect data, display the encoder delta, or
                                display the encoder position. 
           @param keyCommand   ASCII value that corresponds to the character inputted by the user.
           @param update       Tuple value that contains the most recent encoder position and encoder delta values
           @return             True/False value for "zero" attribute.
        '''
        
        
        (pos,del_ticks) = update
        current_time = utime.ticks_ms()
        
        if keyCommand == b'z':
            return True
                
                
        if (self.displayp == True and (keyCommand == b's' or utime.ticks_diff(utime.ticks_add(self.to, self.DisplayPositionTime), current_time) <= 0)):
            print('Time [sec], Position [ticks]')
            for n in range(len(self.tlist)):
                print('{:}, {:}'.format(self.tlist[n]/1000,self.plist[n]))
            self.displayp = False
            self.tlist = []
            self.plist = []
            
        elif(self.displayp):
            self.tlist.append(utime.ticks_diff(current_time, self.to))
            self.plist.append(pos)
        
        elif keyCommand == b'g':
            print('Collecting data...')
            self.displayp = True
            self.to = current_time
            
        elif keyCommand == b'd':
            print('Encoder Delta: ' +str(del_ticks))
            
        elif keyCommand == b'p':
            print('Encoder Position: ' + str(pos))
            
        return False
            
            
    def read(self):
        '''@brief     Method that directly reads the VCP's character input and returns that character input to the run() method, or displays the user interface again.
           @return    Character inputted by the user via puTTY.
        '''
        
        keyCommand = b''  #defining keyCommand so error is not thrown
        
        if(self.CommReader.any()):
            
            keyCommand = self.CommReader.read(1)
            
            self.CommReader.read()
            
            if(keyCommand == b'\x1b'):
                self.transition_to(S0_INIT)
                return ''
            
        return keyCommand
          
    def transition_to(self, new_state):
        '''@brief               Transitions the FSM to a new state
           @param new_state     The next state in the FSM to transition to
        '''
        self.state = new_state
            
