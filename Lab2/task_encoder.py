"""@file        task_encoder.py
   @brief          Module that communicates with the encoders and collects data from the encoders.
   @details        This module is responsible for communication and data sharing with the encoder driver module, encoder.py. It first creates encoder objects for the
                encoders in use, and then is responsible for both zeroing the encoders and collecting data from the encoder at the desired time step. 
   @author        Zachary Rannalli
   @author        Clayton Elwell
   @date           October 21, 2021
"""

#import time
import pyb
import encoder
import utime

## Assigns pin B6 to this attribute
pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
## Assigns pin B7 to this attribute
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)

#pinC6 = pyb.Pin(pyb.Pin.cpu.C6)

#pinC7 = pyb.Pin(pyb.Pin.cpu.C7)

## Constant associated with State 0
S0_INIT = 0

## Constant associated with State 1
S1_WAIT_FOR_INPUT = 1

class Task_Encoder:
    '''@brief       Class that updates encoder object at a requisite interval, zeros the encoder, and collects rotational position data from the encoder.
    
    '''
        
    def __init__(self, period):
        
        '''@brief           Constructs an encoder object
           @param period    Period at which the task_encoder method is executed
        '''
        ## Initiation of one encoder as an object.
        self.encoder1 = encoder.Encoder(pinB6, pinB7, 8) # initiates encoder1 as an object
        #self.encoder2 = encoder.Encoder(pinC6, pinC7, 3) 
        ## The period at which the task_encoder updates the encoder position.
        self.period = period
        ## An integer value representing the current state of the task_encoder FSM.
        self.state = S0_INIT
        ## Timer that increases by an interval of the assigned task_encoder period.
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
        ## Tuple that holds the encoder position and encoder delta values for each update of the encoder.
        self.lastupdate = (0,0)
    
    def run(self, zero):
        '''@brief           Method that updates encoder at a fixed time interval, and zeros the encoder if the Boolean value "zero" is True.
           @param zero      Boolean value that determines if the encoder position should be reset to zero ticks.
        
           @return          Most recent update of the encoder position and encoder delta.        
        '''    
        
        if zero:
        
            self.encoder1.set_position(0)
            
            
        if (utime.ticks_diff(utime.ticks_ms(), self.next_time) >= 0):
            
            self.encoder1.update()
            #self.encoder2.update()
            
            self.lastupdate = self.transition_to()
            
        return self.lastupdate
    
    def transition_to(self):
        '''@brief      Method that adds an interval of one period to the self.next_time attribute and returns the encoder position and encoder delta of the current 
                       update.
           @return     Instantaneous encoder position and encoder delta.
        '''
        self.next_time += self.period
        
        return (self.encoder1.get_position(), self.encoder1.get_delta())
        
                

