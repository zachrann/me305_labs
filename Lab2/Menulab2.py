# -*- coding: utf-8 -*-
"""@file Menulab2.py
@brief Lab 0x02 Incremental Encoders
@mainpage
      


                

@section sec_intro    Introduction
                      In this lab we had to set up an encoder driver and task, as well as create a user interface to interact with the encoders
                      
@section sec_links    Links for lab 2

         Lab 0x02 Finite State Machine: https://imgur.com/a/lgxKuXC 
         
@author  Zachary Rannalli


"""

