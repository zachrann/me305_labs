"""@file        main.py
   @brief       FSM main program
   @details        The main module that utilizes the subsequent task_encoder. and task_user.py modules to communicate with the MAXON motor encoder via the 
                encoder.py module. It runs the task_user and task_py modules at a set frequency that allows for proper update rates of the encoder's position
                which is counted by ticks.
   @author        Zachary Rannalli
   @author        Clayton Elwell
   @date          October 21, 2021
"""

#import task_example
#import time
#import shares

#import pyb
#import time
import task_encoder
import task_user

## Defines the period [ms] (and thus frequency) at which the task_encoder is run. In this case it is currently set at 2 ms.
T_encoder = 2
## Defines the period [ms] (and thus frequency) at which the task_user is run. In this case it is currently set at 10 ms.
T_user = 10

    
if __name__ == '__main__':   
    ''' @brief    Creates objects for the encoder tasks and user tasks, and subsequently updates them at their respectively chosen frequencies.
        
    '''
    ## Encoder task object
    encodertask = task_encoder.Task_Encoder(T_encoder)
    ## User interface task object
    usertask = task_user.Task_User(T_user)
    ## Boolean value that determines whether the encoder should be zeroed or not, based off of user input
    zero = False
    
    while(True):
        
        try:
            ## Tuple attribute that contains the instantaneous encoder position and encoder delta values.
            update = encodertask.run(zero)
            zero = usertask.run(update)
        
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
    ## The task object associated with the LED blinking code
    



   
 

