'''@file       task_motorFINAL.py
   @brief      Task that interfaces with the motor driver
   @details    Interfaces with the motor driver to send duty cycles to the motor.
   @author     Zachary Rannalli
   @author     Clayton Elwell
   @date       December 9, 2021
'''

import utime
import pyb
#import motor_driver
## Attribute associated with Nucleo pin B4
pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
## Attribute associated with Nucleo pin B5
pinB5 = pyb.Pin(pyb.Pin.cpu.B5)
## Attribute associated with Nucleo pin B0
pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
## Attribute associated with Nucleo pin B1
pinB1 = pyb.Pin(pyb.Pin.cpu.B1)



class TaskMotor():
    
    '''@brief      Motor task that executes motor driver methods
       @details    Tracks encoder position and velocity; zeros encoder if ZERO is true
       
    '''
    
    def __init__(self, period, Shares, motor_drv):
        
        '''@brief       Constructs a motor task object.
           @details     Instantiates the period, shared data, and motor driver.
           @param       period The frequency at which the motor is updated within main.py
           @param       Shares The shared data between all the tasks and drivers
           @param       motor_drv  A DRV8847 object
        '''
        
        ## Defines the encoder overflow period
        self.period = period
        
        ## Current time + 1 period
        self.next_time = self.period + utime.ticks_ms()
        
        ## Attribute that communicates with the shared motor array;
        ## [E_N, POS, VEL, DUTY, ZERO, NO_FAULT]
        self.Shares = Shares
        
        ## Attribute associated with the motor driver 
        self.motor_drv = motor_drv
        
        
        # if self.Shares.read(E_N) == 1:
            ## Attribute associated with the motor 1 object
        self.motor1 = self.motor_drv.motor(pinB4, pinB5, 1, 2)
        # elif self.Shares.read(E_N) == 2:
            ## Attribute associated with motor 2 object
        self.motor2 = self.motor_drv.motor(pinB0, pinB1, 3, 4)
            
        #self.motor_drv.enable()
        self.motor1.set_duty(0)
        self.motor2.set_duty(0)
        
    def run(self):
         '''@brief      Motor task that executes motor driver methods
            @details    Tracks encoder position and velocity; zeros encoder if ZERO is true
       
         '''
         
         if (utime.ticks_diff(utime.ticks_ms(), self.next_time) >= 0):
             
             self.next_time += self.period
             
             (duty1, duty2) = self.Shares.read()
             self.motor1.set_duty(duty1)
             self.motor2.set_duty(duty2)
             
             # self.motor.set_duty(self.Shares.read(DUTY))
             