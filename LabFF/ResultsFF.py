'''@file ResultsFF.py

@page FF_page Term Project Report

This report highlights the functionality of the Finite State Machine and Controller that was used for the ball balancing platform in the ME 305 term project.

Here is the task diagram and some task state transition diagrams for our ball-and-plate platform program;
    \image html taskdiagram.JPG "Figure 1. High level task diagram and some FSMs for our ball-and-plate platform project."
    
Here are more state transition diagrams for our ball-and-plate platform hardware and controller:
    \image html fsm.JPG "Figure 2. State transition diagram for our platform's hardware and controller."

Here is a video demonstrating the user interface functionality, as well as the behavior that the controller attempts to driver the ball and plate system towards. <br>
https://www.youtube.com/watch?v=xpG04apApBM&ab_channel=ClaytonE

As seen from the video in the following link: https://www.youtube.com/watch?v=-rCVxf9tghA&ab_channel=ClaytonE, the controller gains were not optimized such that the platform can balance the ball
for a sustained period of time. While the controller that was developed cannot balance the ball for a sustained period of time, our thought process for the controller development follows a
Full State Feedback model. After collecting data from the resistive touch panel, we found that the controller kept the ball on the platform for at most
two seconds. Figure 3 displays a plot of the ball's x and y positions on the resistive touch panel versus time. From Figure 3 it is evident that the controller is not 
able to reach the equilibrium state at which the ball is located at the center of the platform. One important thing to note is that if our platform does not return to a perfectly flat equilibrium
state, then the metal ball will never become stable because its inertia will always carry it away from equilibrium. Figure 4 reveals more details about the platform's
ability to return to a flat equilibrium position.
  \image html ballandplatexy.PNG "Figure 3. Ball x and y touch panel positions versus time."



While the controller was not able to balance the ball, it was able to balance itself. As seen in the following link: https://www.youtube.com/watch?v=oHe4kN9KipQ&ab_channel=ClaytonE, when the 
platform alone is displaced from equilibrium, it returns a relatively flat state. Figure 4 displays a plot of the platform's pitch (rotation about the x-axis)
and roll (rotation about the y-axis) versus time. Figure 4 reveals that the Full State Feedback controller can drive the platform alone to a stable equilibrium point in which it is upright.
  \image html yawandpitch.PNG "Figure 4. Yaw [rad], pitch [rad], yaw rate [rad/s], and pitch rate [rad/s] versus time."

In addition to our platform being able to self-right itself, our data recording functionality is demonstrated in Figures 3 and 4. We additionally wanted to demonstrate
the functionality of our alpha-beta filtering, so we recorded data while tracing a figure-eight pattern on the touch pad with our finger (Figure 5). While this does not highlight
much about the functionality of our controller, it does look cool.
    \image html figure8.PNG "Figure 5. Figure-eight pattern created by plotting y-coordinates versus x-coordinates."

'''
