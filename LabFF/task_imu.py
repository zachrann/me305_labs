'''@file     task_imu.py
   @brief    A module for interfacting with the BNO055 Inertial Measurement Unit
   @details  Collects various pieces of data from the IMU such as Euler Angles and Anguar Velocity. In addition it checks for the calibration coefficients
             to determine if calibration of the IMU is necessary.
   @author     Zachary Rannalli
   @author     Clayton Elwell
   @date     December 9, 2021
   
'''   
import imu_driver
import os
import utime
from pyb import I2C
import time
## Converts from degrees to radians
deg2rad = 3.14159265/180

class TaskIMU:
    '''@brief    IMU task class that interfaces with the IMU
       @details  Collects euler angles and angular velocities. Calibrates IMU.
    '''
    
    def __init__(self, Shares):
        '''@brief    Contructor for IMU task object
           @details  Creates shared object, sets up communication with i2c, and creates an IMU driver object.
           @param    Shares The shared euler angle and angular velocity data
        '''
        
        i2c = I2C(1,I2C.MASTER)
        i2c.init(I2C.MASTER, baudrate = 400000)
        ## IMU driver attribute
        self.imu = imu_driver.BNO055(i2c)
        #print("attribute created")
        ## Attribute that sleeps the module
        self.delay = utime.sleep
        ## Attribute associated with the shared IMU data
        self.Share = Shares
        
        self.readwriteCalibBuf()
    
    def readwriteCalibBuf(self):
        '''@brief   Reads and writes to the calibration coefficients text file.
        @details If there is a file in the directory called "IMU_cal_coefs.txt" then the method reads from this text file and writes the calibration
                    coefficients to the appropriate register. If there is no file named "IMU_cal_coefs.txt" then the method opens a file and writes the
                    current calibration coefficients to that file.
        '''
        filename = 'IMU_cal_coefs.txt'
        try:
            with open(filename, 'r') as f:
                
                p = f.readline()
                #print(p)
                read = [int(k,16) for k in p.strip.split(',')]
                read_buf = bytearray(read)
                self.imu.writeCalibCoeff(read_buf)
                
        except:
            with open(filename, 'w') as f:
                
                read = self.calibrate()
                read = ','.join([hex(k) for k in read])

                f.write(f"{read}\r\n")
            
    def calibrate(self):
        '''@brief   Calibrates the IMU
           @details Prompts the user to move the IMU and subsequently calibrate it.
           @return  The output of the method readCalibCoeff()
        '''
    
        while True:
            cal_status = self.imu.calibStatus()
            print("Calibration Status Value: " + str(cal_status) + "\n")
            
            #self.delay(25)
            time.sleep(.2)
            
            if(cal_status[0]*cal_status[1]*cal_status[2]*cal_status[3]==81):
                #calibrate = False
                print('IMU Calibrated')
                return self.imu.readCalibCoeff()
            
            
    def data(self):
        '''@brief     Collects the euler angles and angular velocities from the IMU driver
           @details   Collects the requested data and writes it to the shared data object.
        '''
        
        
        (yaw, th_x, th_y) = self.imu.eulerAngles()
        (yawd, thd_x, thd_y) = self.imu.angularVel()
        
        self.Share.write( (th_x * deg2rad, th_y * deg2rad, thd_x * deg2rad, thd_y * deg2rad) )
        
        
        
        
        