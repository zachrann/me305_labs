'''@file     task_controller.py
   @brief    A module for executing the controller designed for the platform and ball system
   @details  Interfaces with different sensors to calculate the appropriate duty cycle to balance the ball on the platform.
   @author     Zachary Rannalli
   @author     Clayton Elwell
   @date     December 9, 2021
   
'''   

import utime

from ulab import numpy as np




class Controller:
    '''@brief      Controller object
       @details    Calculates the appropriate duty cycle based on the relationship u = -Kx and input data from the sensors on the platform.
    '''
    
    def __init__(self, period, ballShare, imuShare, dutyShare, stateShare, modeShare):
        '''@brief    Instantiates controller object
           @details  Assigns relevant shared objects to class attributes and defines initial controller parameters
           @param    period  The period at which the controller task runs
           @param    ballShare The shared ball position and velocity data
           @param    imuShare  The shared IMU euler angle and angular velocity data
           @param    dutyShare The shared duty cycle data for each motor
           @param    stateShare The shared data associated with the plate's system state variables
           @param    modeShare  The shared data that determines whether to run the controller or not
        '''
        ## Period at which the controller object runs
        self.period = period
        ## Next time at which the controller should execute the algorithm
        self.nextTime = period + utime.ticks_ms()
        ## Attribute associated with the on-off status of the balancing controller
        self.mode = modeShare
        ## Attribute associated with the shared ball position and velocity data
        self.ball = ballShare
        ## Attribute associated with the shared IMU euler angle and angular velocity data
        self.imu = imuShare
        ## Attribute associated with the shared duty cycles for each motor
        self.duty = dutyShare
        ## Attribute associated with the shared state space data for each motor and each motor's duty cycle
        self.state = stateShare
        
        ## Attribute associated with the gain parameters for the developed controller
        self.K = np.array([[-1.2, -4.5, -500, -0.3]])
    
        R = 2.21
        K_t = 13.8
        V_DC = 12
        ## Attribute that converts controller output torque to motor duty cycle
        self.C = 100*R/(4*K_t*V_DC)
        
    def run(self):
        '''@brief     Runs the control algorithm
        '''
        
        if self.mode.read()[0] == 0:
            self.duty.write((0,0))
        else:
            #print('controlling')
            self.controller()
            
            
    def controller(self):
        '''@brief     Executes control logic
           @details   Uses the control logic u = -Kx to calculate the desired duty cycle based on the closed-loop state vectors
        '''
        
        
        (x, y, xd, yd, z) = self.ball.read()
        (th_x, th_y, thd_x, thd_y) = self.imu.read()
        #print(th_x)
        #print(th_x)
        # if self.Mode.read()[0] == 0:
        #         self.D1c = 0
        #         self.D2c = 0
        
        X1 = np.array([ [x], [xd], [th_y], [thd_y] ])
        X2 = np.array([ [y], [yd], [th_x], [thd_x] ])
        
        T1 = np.dot(-self.K,X1)[0,0]
        T2 = -np.dot(-self.K,X2)[0,0]
        #print(T1)
        #print(T2)
        #print(T1)
        #print(T2)
        
        duty1 = self.C*T1
        duty2 = self.C*T2
        self.duty.write((duty1, duty2))
        
        self.state.write([x, y, xd, yd, th_x, th_y, thd_x, thd_y, duty1, duty2])