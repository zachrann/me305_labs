"""@file       imu_driver.py
@brief         A driver class that allows one to interface with the BNO055 sensor   
@details       This class interacts with the BNO055 Inertial Measurement Unit sensor. This class interfactes with the IMU registers to collect
               accelerometer, gyrometer,and magnetometer data, and subsequently return that data upon request. In addition, this class calibrates the IMU
               and stores the calibration coefficients in a text file.
@author     Zachary Rannalli
@author     Clayton Elwell
@date      December 9, 2021
"""
#import pyb
#from pyb import I2C
import struct
import time
import utime
import os
import pyb
## Clock line
SCL = pyb.Pin(pyb.Pin.cpu.B8)
## Data line
SDA = pyb.Pin(pyb.Pin.cpu.B9)

class BNO055():
    '''@brief     IMU driver class object
       @details   Driver that communicates with the BNO055 registers to collect desired data
    '''
    
    
    def __init__(self, imuObject):
        '''@brief    Constructs the BNO055 IMU class object
           @details  This module creates the IMU object and also creates various buffers to store euler angles, angular velocities, and calibration coefficients.
           @param    imuObject An instance of the BNO055 IMU object 
        '''
        ## IMU object attribute
        self.i2c = imuObject
        ## Buffer created for storing IMU euler angles
        self.eulerbuf = bytearray(6)
        
        #self.calibrationBuff = bytearray(1)
        ## Buffer created for storing IMU calibration coefficients
        self.calibCoeffBuff = bytearray(22)
        ## Buffer created for storing IMU angular velocities
        self.gyroBuff = bytearray(6)
        
        self.operatingMode(0x0C)
        #self.calibrationBuff = self.i2c.mem_read(self.calibrationBuff, 0x28, 0x55)
        

    def operatingMode(self, mode):
        '''@brief    Sets the IMU operating mode
           @details  We want the fused IMU accelerometer, gyroscope, an magnetometer data, so we will using the 9DOF fusion operating mode that is given in the 
                     BNO055 datasheet.
           @param    mode The desired operating mode to be sent to the operating mode register.
        '''
        ## Attribute that resets the operating mode
        self.i2c.mem_write(0x0000, 0x28, 0x3D)
        ## Attribute that writes the operating mode given by the parameter "mode".
        self.i2c.mem_write(mode, 0x28, 0x3D)
    
    def readOM(self):
        '''@brief    Reads the IMU operating mode
           @details  Reads the current operating mode of the IMU from the appropriate register that is given in the BNO055 datasheet.
           
        '''
        ## Attribute that reads the operating mode
        OM = self.i2c.mem_read(1, 0x28, 0x3D)
        print(OM)
        
    def calibStatus(self):
        '''@brief    Reads the caibration status of the IMU
           @details  Interprets the binary calibration status that is sent by the IMU and bit shifts the data so that it is returned as a 1-by-4 base 10 tuple.   
           @return   The 1-by-4 base 10 tuple of calibration statuses.
        '''
        #print('Binary: ', '{:#010b}'.format(self.calibrationBuff[0]))
        buf = bytearray(1)
        self.i2c.mem_read(buf, 0x28, 0x35)
        #for n in range(len(calibrationStatus)):
        #print('Binary: ', '{:#010b}'.format(calibrationStatus[0]))    
         
        
        
        #print("Binary:", '{:#010b}'.format(cal_bytes[0]))

      
        cal_status = ( buf[0] & 0b11,
                      (buf[0] & 0b11 << 2) >> 2,
                      (buf[0] & 0b11 << 4) >> 4,
                      (buf[0] & 0b11 << 6) >> 6)
        
        return cal_status
        
        
    def readCalibCoeff(self):
        '''@brief    Reads the current IMU calibration coefficients
           @details  Reads the current IMU calibration coefficients from the appropriate register as given in the BNO055 datasheet.
           @return   The binary calibration coefficient data from the IMU registers.
        '''
        
        self.i2c.mem_read(self.calibCoeffBuff, 0x28, 0x55)
        
        calibCoeff = struct.unpack('BBBBBBBBBBBBBBBBBBBBBB', self.calibCoeffBuff)
        
        return calibCoeff
        #calibSigned = struct.unpack('<hhhhhhhhhhh', calibCoeff)
        
        #calibScaled = tuple(calib_int/16 for calib_int in calibSigned)
        
        
        #return calibScaled 
        
    def writeCalibCoeff(self, calibCoeff):
        '''@brief     Writes to the IMU calibration coefficient buffer
           @details   Writes the coefficients given by the parameter "calibCoeff" to the appropriate IMU register as given by the BNO055 datasheet.
        '''
        ## Attribute that writes to the BNO055 calibration coefficient registers.
        self.i2c.mem_write(calibCoeff, 0x28, 0x55)
        
    def eulerAngles(self):
        '''@brief    Reads the IMU euler angles
           @details  Interacts with the IMU euler angle registers and unpacks the data to euler angles [deg].
           @return   The scaled euler angle values [deg]
        '''
        euler = self.i2c.mem_read(self.eulerbuf,0x28,0x1A)
       # print("Raw Bytes: ", euler)
        eulerSigned = struct.unpack('<hhh',euler)
       # print('Unpacked: ', eulerSigned)
        (yaw, th_x, th_y) = tuple(euler_int/16 for euler_int in eulerSigned)
        #print((yaw, th_x, th_y))
        #print('Unpacked Euler Angles (Yaw [z] , Roll [x], Pitch [y]): ', eulerScaled)
        #print('poopoo')
        #print(euler) 
        return (yaw, th_x, th_y)
    
    def angularVel(self):
        '''@brief    Reads the IMU angular velocities
           @details  Interacts with the IMU angular velocity registers and unpacks the data to angular velocites [deg/s]
           @return   The scaled angular velocity values [deg/s]
        '''
        
        gyroData = self.i2c.mem_read(self.gyroBuff, 0x28, 0x14)
        #print('Raw Velocity Bytes: ', gyroData)
        gyroSigned = struct.unpack('<hhh', gyroData)
        
        gyroScaled = tuple(gyro_int/16 for gyro_int in gyroSigned)
        
        #print('Unpacked Angular Velocities (X, Y, Z): ', gyroScaled)
        return gyroScaled
        
    def deinit(self):
        '''@brief  Deinitiates the IMU
           @details Uses the built-in deinit() I2C command to deinitiate the IMU.
        '''
        ## Attribute associated with the i2c deinitiate command.
        self.i2c.deinit()
    
    
    

    
            
#if __name__ == '__main__':
    #pyb.I2C(pyb.Pin.cpu.PB8, pyb.Pin.cpu.PB9)
    
    
      
    
    
