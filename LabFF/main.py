"""@file        main.py
   @brief       FSM main program
@details        File imports hardware and user tasks and sets the period at which they update.
@author     Zachary Rannalli
@author     Clayton Elwell
@date          December 9, 2021
"""



import motor_driver
#import imu_driver
#import touch_driver

#import utime
#from ulab import numpy as np
import pyb

import task_userFINAL
import task_motorFINAL
import task_imu
import task_touch
import task_controller
import task_record

import shares

## Defines the period [ms] (and thus frequency) at which the task_encoder is run. In this case it is currently set at 2 ms.

## Defines the period [ms] (and thus frequency) at which the task_user is run. In this case it is currently set at 100 ms.
T_user = 100
## Defines the period [ms] (and thus frequency) at which the task_user is run. In this case it is currently set at 1 ms.
T_motor = 3
## Defines the period [ms] (and thus frequency) at which the task_user is run. In this case it is currently set at 1 ms.
T_control = 3
## Defines the period [ms] (and thus frequency) at which the task_user is run. In this case it is currently set at 5000 ms.
T_record= 5000



if __name__ == '__main__':   
    ''' @brief    Creates objects for the encoder tasks and user tasks, and subsequently updates them at their respectively chosen frequencies.
        
    '''
    ## Share associated with ball position on platform
    ballShare = shares.Share((0,0,0,0,0))
    ## Share for euler angles and angular velocities
    imuShare = shares.Share((0,0,0,0))
    ## Share for duty cycles
    dutyShare = shares.Share((0,0))
    ## Share for system state variables
    stateShare = shares.Share((0,0,0,0,0,0,0,0,0,0))
    ## Share that determines the motor on/off status
    modeShare = shares.Share([0])
    ## Share for recording state variable data
    collectShare = shares.Share([0,0,[0,0,0,0,0,0,0,0,0,0]])
    ## Motor driver object
    motor_drv = motor_driver.DRV8847(3) 
    ## Motor task object
    motorTask = task_motorFINAL.TaskMotor(T_motor, dutyShare, motor_drv)
    ## Touch task object
    touchTask = task_touch.TouchPanel(ballShare)
    ## IMU task object
    imuTask = task_imu.TaskIMU(imuShare)
    #print('imu task created')
    ## CommReader for puTTY communication
    CommReader = pyb.USB_VCP()
    ## Task user object
    userTask = task_userFINAL.TaskUser(T_user, modeShare, stateShare, collectShare, CommReader)
    ## Task controller object
    controlTask = task_controller.Controller(T_control, ballShare, imuShare, dutyShare, stateShare, modeShare)
    ## Task record object
    recordTask = task_record.TaskRecord(T_record, collectShare, stateShare)
   
    while(True):
        
        try: 
            touchTask.data()
            imuTask.data()
            controlTask.run()
            motorTask.run()
            userTask.run()
            recordTask.run()
            
            
            
            
        except KeyboardInterrupt:
            
            break
    
    dutyShare.write((0,0))
    motorTask.run()
    print('Program Terminating')
    ## The task object associated with the LED blinking code
    



   
 

