"""@file task_userFINAL.py
@brief    Module that creates the puTTY user interface.
@details  This module reads the user's input from the keyboard and subsequently inteprets the user input to determine
            whether to balance the encoder and/or collect and display data
@author     Zachary Rannalli
@author     Clayton Elwell
@date    December 9, 2021
"""

#import task_encoder
import utime


## Constant associated with State 0
S0_INIT = 0

## Constant associated with State 1
S1_WAIT_FOR_INPUT = 1


class TaskUser:
    '''@brief   Class that creates the puTTY user interface and facilitates the user's interaction with the various tasks
       @details Class that handles all serial communication between the user and backend. Creates a user interface.
    '''
    
    
    def __init__(self, period, modeShare, stateShare, collectShare, CommReader):
        '''@brief           Method that initiates the necessary attributes for communication with the various tasks
           @param           period    The period at which the task_user class is executed.
           @param           modeShare The shared data that determines whether or not to execute the controller
           @param           stateShare The shared data associated with the system state variables
           @param           collectShare  The shared data associated with the data collection task
           @param           CommReader   Interpreter between the puTTY terminal and Nucleo board
        '''
        
        
        ## The current state of the task_user FSM.
        self.state = S0_INIT
        
        ## Number of iterations of the FSM
        self.runs = 0
        
        ## The frequency at which the task_user class is executed.
        self.period = period
                
        ## Attribute that allows the run() method to determine if the FSM should start.
        self.nexttime = utime.ticks_add(self.period, utime.ticks_ms())
        #print(self.nexttime)
        ## Attribute associated with the controller mode shared object
        self.mode = modeShare
        ## Attribute associated with the shared state variable data and shared duty cycle data
        self.stateShare = stateShare
        ## Attribute associated with the collected data for each motor
        self.collect = collectShare
        ## Attribute associated with the CommReader object
        self.comm = CommReader
        
        ## Attribute that indicates whether or not to print data
        self.print = 0
        
        
        
        
        
    def run(self):
        '''@brief         Method that creates the user interface and FSM that interprets the user's character inputs.
           @details       Transitions through the states once per period and prints to user interface. Once the user is finished the collected data is 
                           prepared and the FSM transitions back to state 1.
            
        '''
        #current_time = utime.ticks_ms()
        
        #print('run')
        #print(utime.ticks_ms())
        
        if utime.ticks_diff(utime.ticks_ms(), self.nexttime) >= 0:
            
            self.nexttime += self.period
            #print(self.state)
            if self.state == S0_INIT:
                print('here')
                #Execute code for state 1
                print("\033c", end="")
                print('--------------------------------\n'
                      '     USER COMMAND INTERFACE     \n'
                      '--------------------------------\n'
                      'P: Print the ball position and platform angle\n'
                      'p: Toggles print display\n'
                      'g: Collect data and print to puTTY\n'
                      's: End data collection prematurely\n'
                      'Enter: Toggle motors on and off\n'
                      'Esc: Display user interface controls\n'
                      '--------------------------------')
                self.transition_to(S1_WAIT_FOR_INPUT)
            elif self.state == S1_WAIT_FOR_INPUT:
                keyCommand = self.read()
                self.write(keyCommand[0])
           
    
    def write(self,keyCommand):
        '''@brief              Method that interprets the user input via keyCommand and subsequently determines whether to collect data or activate balancing.
           @details            Contains logic for user input recognition, such as printing position or collecting data.
           @param              keyCommand ASCII value that corresponds to the character inputted by the user.
           
        '''
        
        if self.print or keyCommand == b'P'[0]:
            (x, y, xd, yd, th_x, th_y, thd_x, thd_y, duty1, duty2 ) = self.stateShare.read()
            print("\033c_________Ball and Platform Data_________\n\n"
                  "Ball    :    x   = {:.2f}mm,\t\ty   = {:.2f}mm\n"
                  "Platform:    thx = {:.2E}rad,\t\tthy = {:.2E}rad\n"
                  "Duty    :    D1 = {:.2f}%,\t\tD2 = {:.2f}%\n".format(x,y,th_x,th_y,duty1,duty2),end="")
        
        if keyCommand == b'\x1b'[0]:
            self.transition_to(S0_INIT)
            
        elif keyCommand == b'p'[0]:
            self.print ^= 1
            
        elif keyCommand == b'g'[0]:
            self.collect.write([.05,25,[1,1,0,0,0,0,0,0,0,0]])
            print('Recording data...')
            
        elif keyCommand == b's'[0]:
            self.collect.read()[1] = -1
            print('Stopped recording...')
            
        elif keyCommand == 13:
            self.mode.read()[0] ^= 1
            print('Motor Mode: {:}'.format(self.mode.read()[0]))
      
    def read(self):
        '''@brief     Method that directly reads the VCP's character input and returns that character input to the run() method, or displays the user interface again.
           @return    Character inputted by the user via puTTY.
        '''
        
        #keyCommand = b''  #defining keyCommand so error is not thrown
        
        if(self.comm.any()):
            
            keyCommand = self.comm.read(1)
            
            self.comm.read()
             
            
            return keyCommand
            
        return b' '
          

       
           
   
    
    def transition_to(self, new_state):
        '''@brief               Transitions the FSM to a new state
           @param new_state     The next state in the FSM to transition to
        '''
        self.state = new_state
            

   
         
        
        
 
     