# -*- coding: utf-8 -*-
"""@file Menulabff.py
@brief ME 305 Term Project
@mainpage
      


                

@section sec_intro    Introduction
                      Links to the repository of various source codes and Lab documentation
                      
@section sec_links    Links for Project
Control Description and User Interface Demonstration: https://www.youtube.com/watch?v=xpG04apApBM&ab_channel=ClaytonE <br>
Plate Balancing Demonstration: https://www.youtube.com/watch?v=oHe4kN9KipQ&ab_channel=ClaytonE <br>
Ball Balancing Demonstration: https://www.youtube.com/watch?v=-rCVxf9tghA&ab_channel=ClaytonE <br>
@ref FF_page

@author Zach Rannalli
"""
