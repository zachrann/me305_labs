"""@file task_record.py
@brief    Module that records ball position data and platform angle data.
@details  This module records values and prints those respective values
@author     Zachary Rannalli
@author     Clayton Elwell
@date    December 9, 2021
"""

#import task_encoder
import utime
import array

## Constant associated with State 0
S0_INIT = 0

## Constant associated with State 1
S1_WAIT_FOR_INPUT = 1


class TaskRecord:
    '''@brief   Class that records and prints data
       @details Recorded data is specificed in the user task, in addition to the amount of time to record data for.
    '''
    
    def __init__(self, period, collect, stateShare):
        '''@brief   Constructs a TaskRecord object and creates various attributes needed for data recording
           @details Instantiates necessary shared variables and period values
           @param   period The period at which the data is recorded
           @param   collect Indicates what data should be collected and for how long
           @param   stateShare A shared object containing the system state variables
           
        '''
        ## Period during which data is not being collected
        self.invperiod = period
        ## Period during which data is being collected
        self.period = period
        ## Next time interval to collect data
        self.nextTime = utime.ticks_add(self.period, utime.ticks_ms())
        ## Current state
        self.state = S0_INIT
        ## Time at which data recording is equal to zero
        self.t = 0
        ## Recording index
        self.index = [0]  
        ## Counter for determining file names
        self.n = 0
        ## Indicates what data to collect and for how long
        self.collect = collect
        ## Shared object containing system state variables
        self.stateShare = stateShare
        
    def run(self):
        '''@brief    Runs the data recording task
           @details  The collect attribute indicates when to begin collecting data. Once the data collection is complete the collected data is printed and saved
                     to a text file
        '''
        
        if utime.ticks_ms() >= self.nextTime:
            #print(self.collect.read()[1])
            if self.state == S0_INIT:
                if self.collect.read()[1]>0:
                    #print('got here')
                    self.period = self.collect.read()[0]*1000
                    self.collect.read()[0] = 0
                     
                    self.setup()
                    self.t = self.nextTime
                    self.record()
                    self.transition_to(S1_WAIT_FOR_INPUT)
                    #print(self.state)
            elif self.state == S1_WAIT_FOR_INPUT:
                 self.record() 
                 self.collect.read()[1] -= self.period/1000
                 #print(self.collect.read()[1])
                 if self.collect.read()[1] <= 0 or self.collect.read()[0] > 0:
                     
                     self.period = self.invperiod
                     self.printD()
                     self.transition_to(S0_INIT)
                     
            self.nextTime += self.period
            #print(self.nextTime)
        #print(utime.ticks_ms())
    def setup(self):
        '''@brief Sets up data recording lists
        '''
        
        data = self.collect.read()[2]
        
        self.index = [i for i, element in enumerate(data) if element!=0]
        print(self.index)
        with open('xy.txt', 'a') as f:
            f.write("Data:\n")
    
    def record(self):
        '''@brief Records data depending on the state of the FSM
        '''
        time = (self.nextTime-self.t)/100//.01/100
        
        d = [0,0]
        j = 0
        for i in self.index:
            print(i)
            
            print(j)
            d[j] = self.stateShare.read()[i]
            j+=1
        #print('now we are here')    
        self.save(time,d)
        
    def printD(self):
        '''@brief Records data depending on the state of the FSM
        '''
        print('Data collection finished...')
        
    def save(self, t, d):
        '''@brief Saves the recorded data to text file
           @param t The time interval that the data was recorded for
           @param d The recorded data
        '''
        
        with open("xy.txt", 'a') as f:
            a = ",".join(map(str,d))
            data = "{:}, {:}".format(t,a)
            f.write(data+'\n')
            print(data)
    
            
    def transition_to(self, new_state):
        '''@brief               Transitions the FSM to a new state
           @param new_state     The next state in the FSM to transition to
        '''
        self.state = new_state
            