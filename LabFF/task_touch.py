'''@file     task_touch.py
   @brief    A module for interfacting with the Resistive Touch Panel
   @details  Interfaces with touch panel driver to collect the ball's position and velocity on the touch panel. Writes this data to the shared object.
   @author     Zachary Rannalli
   @author     Clayton Elwell
   @date     December 9, 2021
   
'''   

import utime
import touch_driver
from ulab import numpy as np
import os
## Desired calibration points
actualPoints = np.array([[-88,50], [0,50], [88,50], [88,0], [88,-50], [0,-50], [-88,-50], [-88,0], [0,0]])


class TouchPanel:
    '''@brief      Interfaces with touch panel driver
       @details    Assigns the various touch panel pins so that the proper data measurements may be taken from the resistive touch panel.
    '''
    
    def __init__(self, Share):
        
        '''@brief    Constructs a touch panel object
           @details  Constructs panel object and sets initial position and velocity conditions. Also calibrates the touch panel
           @param    Share The shared ball position and velocity data
        '''
        ## Touch Panel Object
        self.touch = touch_driver.TouchPanel()
        
        ## Attribute that allows one to note the time on the timer 
        self.setTime= utime.ticks_us  
        
        (self.x,self.y,self.z) = (0,0,False)
        ## Attribute associated with the y-axis velocity of the ball
        self.Vy = 0
        ## Attribute associated with the x-axis velocity of the ball
        self.Vx = 0
        ## Shared position and velocity data attribute
        self.Share = Share
        
        cal = self.readCalCoeff()
        self.touch.setCal(cal)    
    
    def readCalCoeff(self):
        '''@brief   Reads and writes to the touch panel calibration coefficient text file
           @details If there is no text file in the directory, then the method calls the calibration function and writes the new calibration gains to the
                    text file. If there is a text file in the directory then the method reads from the current values in the text file.
           @return  A tuple containing the platform calibration coefficients.
        '''
        
        filename = 'RT_cal_coefs.txt'
        #path = 'D:\\'
        try:
            with open(filename, 'r') as f:
                #print('test 1')
                cal = f.readline()
                #print(cal)
                cal = tuple([float(cal_idx) for cal_idx in cal.strip().split(',')])
                
                
        except:
            with open(filename, 'w') as f:
                #print('test 2')
                cal = self.execCal(actualPoints)
                (Kxx, Kxy, Kyx, Kyy, x0, y0) = cal
                f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {x0}, {y0}\r\n")
        
        return cal
    
    def execCal(self, actualPoints):
        '''@brief    Calibrates the touch panel
           @details  Takes multiple touches from the user to calibrate the touch panel's x and y axes.
           @param    actualPoints The location of the calibration point as defined by the physical grid.
           @return   A tuple of the calibration matrix values
        '''
        calMatrix = np.ones((9,3))
        ADCMatrix = np.ones((9,2))
        i = 0
        f = False
        #print('before touchey')
        while(i<9):
            (ADCx,ADCy,z) = self.touch.allScan()
            #print((ADCx, ADCy, z))
            #print(f)
            #time.sleep(2)
            if z and f:
                    
                ADCMatrix[i,:] = [ADCx,ADCy]
                f = False
                i += 1
         #       print(ADCx)
          #      print(ADCy)
            elif z == False and f == False:
                f = True
                utime.sleep_us(200000)
                print("Press point ({}, {})".format(actualPoints[i,0], actualPoints[i,1]))
                
        calMatrix[:,0:2] = ADCMatrix
        
        #print(calMatrix)
        calT = calMatrix.transpose()
        #print(np.dot(calT, calMatrix))
        calibMatrix = np.dot( np.linalg.inv( np.dot(calT, calMatrix) ), np.dot( calT, actualPoints ) ) 
        #print(calibMatrix)
        
        return (calibMatrix[0,0], calibMatrix[0,1], calibMatrix[1,0], calibMatrix[1,1], calibMatrix[2,0], calibMatrix[2,1])
    
    
    def contact(self):
        '''@brief   Collects x, y, Vx, and Vy values.
           @details Interprets the Boolean variable "z" to determine if there is contact with the plate. If there was previously no contact with the plate
           then the position attribute are set to the current position and the velocities are set to zero. If there is continuous contact with the plate then
           the alpha-beta filter is employed to determine the position and velocity of the contact point. If there is no contact then the positions and velocities
           are set to zero.
        '''
        
        (x,y,z) = self.touch.allScan()
        if not self.z and z:
            ## Attribute associated with ball x-axis position
            self.x = x
            self.Vx = 0
            ## Attribute associated with ball y-axis position
            self.y = y
            self.Vy = 0
            ## Attribute associated with ball z-axis contact
            self.z = z
            ## Attribute associated with the time elapsed
            self.t = self.setTime()      
            #print('first loop')
        elif z:
            #print('second loop')
            #print(x)
            #print(y)
            #print(z)
            alpha = 0.85
            beta = 0.005
            T = utime.ticks_diff(self.setTime(), self.t )/1E6
            self.t = self.setTime()
            x_current = self.x
            y_current = self.y
            
            self.x = x_current+alpha*(x - x_current) + T*self.Vx
            self.y = y_current+alpha*(y - y_current) + T*self.Vy
            self.Vx = self.Vx+beta/T*(x - x_current)
            self.Vy = self.Vy+beta/T*(y - y_current)
        else:
            self.z = False
            self.x = 0
            self.Vx = 0
            self.y = 0
            self.Vy = 0
            
    def data(self):
        '''@brief   Updates and shares ball translational position and velocity data
        '''
         
        self.contact()
        self.Share.write((self.x, self.y, self.Vx, self.Vy, self.z))