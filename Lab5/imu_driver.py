"""@file       imu_driver.py
@brief         A driver class that allows one to interface with the BNO055 sensor   
@details       This class interacts with the BNO055 Inertial Measurement Unit sensor. This class interfactes with the IMU registers to collect
               accelerometer, gyrometer,and magnetometer data, and subsequently return that data upon request. In addition, this class calibrates the IMU
               and stores the calibration coefficients in a text file.
@author    Zachary Rannalli
@author    Clayton Elwell
"""
#import pyb
#from pyb import I2C
import struct
import time
import utime
import os
import pyb

SCL = pyb.Pin(pyb.Pin.cpu.B8)
SDA = pyb.Pin(pyb.Pin.cpu.B9)

class BNO055():
    '''@brief
           @details
    '''
    
    
    def __init__(self, imuObject):
        '''@brief    Constructs the BNO055 IMU class object
           @details  This module creates the IMU object and also creates various buffers to store euler angles, angular velocities, and calibration coefficients.
           @param    imuObject An instance of the BNO055 IMU object 
        '''
        ## IMU object attribute
        self.i2c = imuObject
        ## Buffer created for storing IMU euler angles
        self.eulerbuf = bytearray(6)
        
        #self.calibrationBuff = bytearray(1)
        ## Buffer created for storing IMU calibration coefficients
        self.calibCoeffBuff = bytearray(22)
        ## Buffer created for storing IMU angular velocities
        self.gyroBuff = bytearray(6)
        
        #self.calibrationBuff = self.i2c.mem_read(self.calibrationBuff, 0x28, 0x55)
        ## Attribute that allows one to read and write to the Calibration Coefficient buffer within this class.                
        self.readwriteCalibBuf()

    def operatingMode(self, mode):
        '''@brief    Sets the IMU operating mode
           @details  We want the fused IMU accelerometer, gyroscope, an magnetometer data, so we will using the 9DOF fusion operating mode that is given in the 
                     BNO055 datasheet.
           @param    mode The desired operating mode to be sent to the operating mode register.
        '''
        ## Attribute that resets the operating mode
        self.i2c.mem_write(0x0000, 0x28, 0x3D)
        ## Attribute that writes the operating mode given by the parameter "mode".
        self.i2c.mem_write(mode, 0x28, 0x3D)
    
    def readOM(self):
        '''@brief    Reads the IMU operating mode
           @details  Reads the current operating mode of the IMU from the appropriate register that is given in the BNO055 datasheet.
           
        '''
        ## Attribute that reads the operating mode
        OM = self.i2c.mem_read(1, 0x28, 0x3D)
        print(OM)
        
    def calibStatus(self):
        '''@brief    Reads the caibration status of the IMU
           @details  Interprets the binary calibration status that is sent by the IMU and bit shifts the data so that it is returned as a 1-by-4 base 10 tuple.   
           @return   The 1-by-4 base 10 tuple of calibration statuses.
        '''
        #print('Binary: ', '{:#010b}'.format(self.calibrationBuff[0]))
        buf = bytearray(1)
        self.i2c.mem_read(buf, 0x28, 0x35)
        #for n in range(len(calibrationStatus)):
       # print('Binary: ', '{:#010b}'.format(calibrationStatus[0]))    
        
        
        
       # print("Binary:", '{:#010b}'.format(cal_bytes[0]))

        cal_status = ( buf[0] & 0b11,
                      (buf[0] & 0b11 << 2) >> 2,
                      (buf[0] & 0b11 << 4) >> 4,
                      (buf[0] & 0b11 << 6) >> 6)

        #print("Values:", cal_status)
        #print('\n')
        return cal_status
        # return calibrationStatus
        
    def readCalibCoeff(self):
        '''@brief    Reads the current IMU calibration coefficients
           @details  Reads the current IMU calibration coefficients from the appropriate register as given in the BNO055 datasheet.
           @return   The binary calibration coefficient data from the IMU registers.
        '''
        
        calibCoeff = self.i2c.mem_read(self.calibCoeffBuff, 0x28, 0x55)
        return calibCoeff
        #calibSigned = struct.unpack('<hhhhhhhhhhh', calibCoeff)
        
        #calibScaled = tuple(calib_int/16 for calib_int in calibSigned)
        
        
        #return calibScaled 
        
    def writeCalibCoeff(self, calibCoeff):
        '''@brief     Writes to the IMU calibration coefficient buffer
           @details   Writes the coefficients given by the parameter "calibCoeff" to the appropriate IMU register as given by the BNO055 datasheet.
        '''
        ## Attribute that writes to the BNO055 calibration coefficient registers.
        self.i2c.mem_write(calibCoeff, 0x28, 0x55)
        
    def eulerAngles(self):
        '''@brief    Reads the IMU euler angles
           @details  Interacts with the IMU euler angle registers and unpacks the data to euler angles [deg].
           @return   The scaled euler angle values [deg]
        '''
        euler = self.i2c.mem_read(self.eulerbuf,0x28,0x1A)
       # print("Raw Bytes: ", euler)
        eulerSigned = struct.unpack('<hhh',euler)
       # print('Unpacked: ', eulerSigned)
        (yaw, th_x, th_y) = tuple(euler_int/16 for euler_int in eulerSigned)
        print((yaw, th_x, th_y))
        #print('Unpacked Euler Angles (Yaw [z] , Roll [x], Pitch [y]): ', eulerScaled)
        #print('poopoo')
        #print(euler) 
        return (yaw, th_x, th_y)
    
    def angularVel(self):
        '''@brief    Reads the IMU angular velocities
           @details  Interacts with the IMU angular velocity registers and unpacks the data to angular velocites [deg/s]
           @return   The scaled angular velocity values [deg/s]
        '''
        
        gyroData = self.i2c.mem_read(self.gyroBuff, 0x28, 0x14)
        #print('Raw Velocity Bytes: ', gyroData)
        gyroSigned = struct.unpack('<hhh', gyroData)
        
        gyroScaled = tuple(gyro_int/16 for gyro_int in gyroSigned)
        
        #print('Unpacked Angular Velocities (X, Y, Z): ', gyroScaled)
        return gyroScaled
        
    def deinit(self):
        '''@brief  Deinitiates the IMU
           @details Uses the built-in deinit() I2C command to deinitiate the IMU.
        '''
        ## Attribute associated with the i2c deinitiate command.
        self.i2c.deinit()
    
    
    def readwriteCalibBuf(self):
        '''@brief   Reads and writes to the calibration coefficients text file.
           @details If there is a file in the directory called "IMU_cal_coefs.txt" then the method reads from this text file and writes the calibration
                    coefficients to the appropriate register. If there is no file named "IMU_cal_coefs.txt" then the method opens a file and writes the
                    current calibration coefficients to that file.
        '''
        filename = 'IMU_cal_coefs.txt'
        if filename in os.listdir():
            with open(filename, 'r') as f:
                
                p = f.readline()
                print(p)
                read = [int(k,16) for k in p.strip.split(',')]
                read_buf = bytearray(read)
                self.writeCalibCoeff(read_buf)
                
        else:
            with open(filename, 'w') as f:
                
                read = self.calibrate()
                read = ','.join([hex(k) for k in struct.unpack('BBBBBBBBBBBBBBBBBBBBBB', read)])

                f.write(f"{read}\r\n")

    def calibrate(self):
        '''@brief   Calibrates the IMU
           @details Prompts the user to move the IMU and subsequently calibrate it.
           @return  The output of the method readCalibCoeff()
        '''
        
        while True:
            cal_status = self.calibStatus()
            print("Calibration Status Value: " + str(cal_status) + "\n")
    
            time.sleep(.25)
            
            if(cal_status[0]*cal_status[1]*cal_status[2]*cal_status[3]==81):
                #calibrate = False
                print('IMU Calibrated')
                return self.readCalibCoeff()
            
#if __name__ == '__main__':
    #pyb.I2C(pyb.Pin.cpu.PB8, pyb.Pin.cpu.PB9)
    
    
      
    
    
