"""@file    main.py
@brief   Main file for imu Lab
@author Zach Rannalli
"""
import pyb
from pyb import I2C
import imu_driver
import time

if __name__ == '__main__':

    i2c = I2C(1)
    i2c.init(I2C.MASTER) 

    test = imu_driver.BNO055(i2c)

    test.operatingMode(0b00001100)
  #  time.sleep(3)
  #  test.readOM()
  #  time.sleep(3)
   # test.calibStatus()
  #  time.sleep(3)
   # test.readCalibCoeff()
   # time.sleep(3)
    #test.writeCalibCoeff()

    calibrate = True
    print('Calibrating IMU; Four 3s Indicates Calibration')
    while(calibrate):
        try:
            
            status = test.calibStatus()
            print("Calibration Status Value: " + str(status) + "\n")
    
            time.sleep(1)
            
            if(status[0]*status[1]*status[2]*status[3]==81):
                calibrate = False
                print('IMU Calibrated')
                OGcoeff = test.readCalibCoeff()
                print("Calibration Coefficients: " + str(OGcoeff))
                
                while(True):
                    try:
                        test.eulerAngles()
                        time.sleep(1)
                    except KeyboardInterrupt:
                        break
        except KeyboardInterrupt:
            break
    test.deinit()
    print('Program Terminating...')
    
 #  else:
     #   try:
        #print('poo')
   #
        #    test.angularVel()
        
        
    