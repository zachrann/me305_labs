'''@file ResultsLab3.py

@page lab_3_page Task Diagram, FSMs, and Plots for Lab 3

Here is a plot of the angular position and angular velocity versus time with a PWM duty cycle of 100%:
                \image html pandv2.PNG "Angular Position and Velocity Plot"

Here is the Task Diagram and respective FSM's for Lab 3.
                \image html lab3fsm.JPG "Lab 3 Task Diagram and FSMs"

'''


