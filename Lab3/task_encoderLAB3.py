"""@file        task_encoderLAB3.py
   @brief          Module that communicates with the encoders and collects data from the encoders.
   @details        This module is responsible for communication and data sharing with the encoder driver module, encoder.py. It first creates encoder objects for the
                encoders in use, and then is responsible for both zeroing the encoders and collecting data from the encoder at the desired time step. 
   @author         Zach Rannalli
   @author         Clayton Elwell
   @date           November 18, 2021
"""

#import time
import pyb
import encoder_lab3
import utime
import math


## Assigns pin B6 to this attribute
pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
## Assigns pin B7 to this attribute
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
## Assigns pin C6 to this attribute
pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
## Assigns pin C7 to this attribute
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)

## Constant associated with State 0
S0_INIT = 0

## Constant associated with State 1
S1_WAIT_FOR_INPUT = 1

#Index references for shared motor array
## Encoder number
E_N = 0
## Encoder Position
POS = 1
## Encoder Velocity
VEL = 2
## Assigned Duty Cycle
#DUTY = 3
## Encoder zeroing Boolean Value
ZERO = 4
## Converts ticks to radians
tick2rad = (2*math.pi/1919)

class TaskEncoder:
    '''@brief       Class that updates encoder object at a requisite interval, zeros the encoder, and collects rotational position data from the encoder.
    
    '''
        
    def __init__(self, period, Shares):
        
        '''@brief           Constructs an encoder object
           @param           period    Period at which the task_encoder method is executed
           @param           Shares    Shared motor info array
        '''
        
        ## The period at which the task_encoder updates the encoder position.
        self.period = period*1000
        
        ## Timer that increases by an interval of the assigned task_encoder period.
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
       
        ## Timer that represents the last time an update has occured
        self.last_time = utime.ticks_us()
        ## Instantiates the shared data between the task user and task encoder
        self.Shares = Shares
        
        if self.Shares.read(E_N) == 1:
            ## Instantiation of an encoder object
            self.encoder = encoder_lab3.Encoder(pinB6, pinB7, 4)
            
        elif self.Shares.read(E_N) == 2:
            
            self.encoder = encoder_lab3.Encoder(pinC6, pinC7, 8)
            
        
        
        
    
    def run(self):
        '''@brief           Method that updates encoder at a fixed time interval, and zeros the encoder if the Boolean value "zero" is True.
           
        '''    
        
                   
            
        if (utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0):
            
            self.nextUpdate()
            
            if self.Shares.read(ZERO):
                #print('should zero now')
                self.encoder.set_position(0)
                self.Shares.write(ZERO, False)
            
    
    
    def nextUpdate(self):
        ''' @brief     Updates the encoder position and velocity, and sets attribute "next_time" to the next update time.
            
        '''
        tdelta = utime.ticks_diff(utime.ticks_us(), self.last_time)
        self.encoder.update()
        self.next_time += self.period
        self.last_time = utime.ticks_us()
        
        self.Shares.write(POS,self.encoder.get_position()*tick2rad)
        self.Shares.write(VEL,self.encoder.get_delta()*tick2rad/ ((self.period + tdelta)/1000000))
    
    def transition_to(self):
        '''@brief      Method that adds an interval of one period to the self.next_time attribute and returns the encoder position and encoder delta of the current 
                       update.
           @return     Instantaneous encoder position and encoder delta.
        '''
        self.next_time += self.period
        
        return (self.encoder1.get_position(), self.encoder1.get_delta())
        
                

