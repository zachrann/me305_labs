# -*- coding: utf-8 -*-
"""@file Menulab3.py
@brief Lab 0x03 PMDC Motors
@mainpage
      


                

@section sec_intro    Introduction
                      In this lab we had to create a motor driver and task, and create a user interface where we can change the duty cycle.
                      
         
@author  Zachary Rannalli

@ref lab_3_page
"""

