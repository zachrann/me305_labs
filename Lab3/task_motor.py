'''@file       task_motor.py
   @brief      Task assigns hardware pins to motors, checks for user input to disable a fault, and interacts with the motor
               driver to set the duty cycle.
   @details    Correlates each motor to its appropriate pins on the Nucleo, and sets up motor channels. The motor is subsequently enabled
               and an initial duty cycle is sent to the shared data. Sets the motor duty cycle as determined by the user input, and 
               checks to see if the user wants to disable a motor fault.
   @author     Zach Rannalli
   @author     Clayton Elwell
   @date       November 18, 2021
'''

import utime
import pyb
#import motor_driver

## Motor 1 pin 1
pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
## Motor 1 pin 2
pinB5 = pyb.Pin(pyb.Pin.cpu.B5)
## Motor 2 pin 1
pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
## Motor 2 pin 2
pinB1 = pyb.Pin(pyb.Pin.cpu.B1)

#Index references for shared motor array
## Encoder number
E_N = 0
## Encoder Position
POS = 1
## Encoder Velocity
VEL = 2
## Assigned Duty Cycle
DUTY = 3
## Encoder zeroing Boolean Value
ZERO = 4
## Disables motor fault trigger
NO_FAULT = 5

class TaskMotor():
    
    '''@brief      Motor task that executes motor driver methods
       @details    Tracks encoder position and velocity; zeros encoder if ZERO is true
       
    '''
    
    def __init__(self, period, Shares, motor_drv):
        
        '''@brief       Constructs a motor task object.
           @details     Instantiates the period, shared data, and motor driver.
           @param       period The frequency at which the motor is updated within main.py
           @param       Shares The shared data between all the tasks and drivers
           @param       motor_drv  A DRV8847 object
        '''
        
        ## Defines the encoder overflow period
        self.period = period
        
        ## Current time + 1 period
        self.next_time = self.period + utime.ticks_ms()
        
        ## Attribute that communicates with the shared motor array;
        ## [E_N, POS, VEL, DUTY, ZERO, NO_FAULT]
        self.Shares = Shares
        
        ## Attribute associated with the motor driver 
        self.motor_drv = motor_drv
        
        
        if self.Shares.read(E_N) == 1:
            ## Attribute associated with the motor object
            self.motor = self.motor_drv.motor(pinB4, pinB5, 1, 2)
        elif self.Shares.read(E_N) == 2:
            
            self.motor = self.motor_drv.motor(pinB0, pinB1, 3, 4)
        
        
        self.motor_drv.enable()
        self.motor.set_duty(self.Shares.read(DUTY))
        
    def run(self):
         '''@brief      Motor task that executes motor driver methods
            @details    Tracks encoder position and velocity; zeros encoder if ZERO is true
       
         '''
         
         if (utime.ticks_diff(utime.ticks_ms(), self.next_time) >= 0):
             
             self.next_time += self.period
             if(self.Shares.read(NO_FAULT)):
                 self.motor_drv.enable()
                 #print('Motor fault has been disabled')
                 self.Shares.write(DUTY, 0)
                 self.Shares.write(NO_FAULT, False)
             self.motor.set_duty(self.Shares.read(DUTY))
             