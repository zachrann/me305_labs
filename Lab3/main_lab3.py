"""@file        main_lab3.py
   @brief       FSM main program
   @details        The main module that utilizes the subsequent task_encoder. and task_user.py modules to communicate with the MAXON motor encoder via the 
                encoder.py module. It runs the task_user and task_py modules at a set frequency that allows for proper update rates of the encoder's position
                which is counted by ticks.
                
                
   @author         Zach Rannalli
   @author         Clayton Elwell
   @date          October 21, 2021
"""


import task_encoderLAB3
import motor_driver
import task_motor
import task_userLAB3
import shares

## Defines the period [ms] (and thus frequency) at which the task_encoder is run. In this case it is currently set at 2 ms.
T_encoder = 2
## Defines the period [ms] (and thus frequency) at which the task_user is run. In this case it is currently set at 10 ms.
T_user = 40
## Defines the period [ms] (and thus frequency) at which the task_user is run. In this case it is currently set at 10 ms.
T_motor = 2

#Index references for shared motor array
# Encoder number
#E_N = 0
# Encoder Position
#POS = 1
# Encoder Velocity
#VEL = 2     
# # Assigned Duty Cycle
# DUTY = 3
# # Encoder zeroing Boolean Value
# ZERO = 4
# # Disables motor fault trigger
# NO_FAULT = 5
    
if __name__ == '__main__':   
    ''' @brief    Creates objects for the encoder tasks and user tasks, and subsequently updates them at their respectively chosen frequencies.
        
    '''
    ## Motor 1 Shared Values 
    ##
    ##Shared values include the encoder number, encoder position, encoder velocity, assigned duty cycle, zero flag, and fault flag
    ShareM1 = shares.ShareMotor([1, 0, 0, 0, False, False])
    ## Motor 2 Shared Values 
    ##
    ##Shared values include the encoder number, encoder position, encoder velocity, assigned duty cycle, zero flag, and fault flag
    ShareM2 = shares.ShareMotor([2, 0, 0, 0, False, False])
    ## Instantiated motor driver object
    motor_drv = motor_driver.DRV8847(3)
    ## Instantiate encoder task 1 object
    encoderTask1 = task_encoderLAB3.TaskEncoder(T_encoder, ShareM1)
    ## Instantiate encoder task 2 object
    encoderTask2 = task_encoderLAB3.TaskEncoder(T_encoder, ShareM2)
    ## Instantiate motor task 1 object
    motorTask1 = task_motor.TaskMotor(T_motor, ShareM1, motor_drv)
    ## Instantiate motor task 2 object
    motorTask2 = task_motor.TaskMotor(T_motor, ShareM2, motor_drv)
    ## Instantiate user task
    userTask = task_userLAB3.TaskUser(T_user, ShareM1, ShareM2)
    
    while(True):
        
        try: 
            
            encoderTask1.run()
            encoderTask2.run()
            
            userTask.run()
            
            motorTask1.run()
            motorTask2.run()
            
            
            
        except KeyboardInterrupt:
            break
    motor_drv.disable()
    print('Program Terminating')
    ## The task object associated with the LED blinking code
    



   
 

