"""@file task_userLAB3.py
   @brief    Module that creates the puTTY user interface.
   @details  This module reads the user's input from the keyboard and subsequently inteprets the user input to determine
            whether to zero the encoder, display the encoder position or encoder delta, or collect encoder position data and
            display that data on the puTTY virtual command prompt.
   @author   Zach Rannalli
   @author   Clayton Elwell
   @date    November 18, 2021
"""

#import task_encoder
import utime
import pyb
import array as arr

## Constant associated with State 0
S0_INIT = 0

## Constant associated with State 1
S1_WAIT_FOR_INPUT = 1

#Index references for shared motor array
## Encoder number
EN = 0
## Encoder Position
POS = 1
## Encoder Velocity
VEL = 2
## Assigned Duty Cycle
DUTY = 3
## Encoder zeroing Boolean Value
ZERO = 4
## Disables motor fault trigger
NO_FAULT = 5

## Attribute that allows the method to communuicate with the USB's virtual command prompt.
CommReader = pyb.USB_VCP()

class TaskUser:
    '''@brief   Class that creates the puTTY user interface and facilitates the user's interaction with the encoder via the task_encoder module.
       @details Handles all communication with the VCP and backend data collection. Creates a UI for the user within the VCP.
    '''
    
    
    def __init__(self, period, Share1, Share2):
        '''@brief           Method that initiates the necessary attributes for communication with the task_encoder and the various lists and timers to collect the encoder data and display it in the puTTY 
                            terminal.
           @details         Instantiates a period and the shared data for each motor.
           @param           period    The period at which the task_user class is executed.
           @param           Share1   Shared variable associated with the first motor
           @param           Share2   Shared variable associated with the second motor
        '''
        
        
        ## The current state of the task_user FSM.
        self.state = S0_INIT
        
        ## Number of iterations of the FSM
        self.runs = 0
        
        ## The frequency at which the task_user class is executed.
        self.period = period
                
        ## Attribute that allows the run() method to determine if the FSM should start.
        self.nexttime = utime.ticks_add(self.period, utime.ticks_ms())
        
        
        #self.UserCOMM = True
        ## Boolean value that determines if the write() module should begin collecting data from the encoder.
        self.displayp = [False, False]
        
      
        ## 30 second value that allows the write() module to determine if data collection is done.
        self.DisplayPositionTime = 30010
        ## Initial time value that is used with self.DisplayPositionTime to determine if data collection is done.
        self.to = 0
        
        ## Attribute associated with the first motor share.
        self.Share1 = Share1
        ## Attribute associated with the second motor share.
        self.Share2 = Share2
        
        ## Boolean for controlling if user can change the motor duty cycle
        self.initDuty = [False, False]
        ## List containing integers for the user input duty cycle.
        self.zeroshare = [0,0]
        
        ## Variable to track time
        self.time = utime.ticks_ms()
        
        ## Boolean for controlling position display
        #self.initPos = [False, False]
        
        ## Array to collect time data from both motor/encoder combos
        self.tArray = [arr.array('f',[]), arr.array('f',[])]
        
        ## Array to collect position data from both encoders
        self.pArray = [arr.array('f',[]), arr.array('f',[])]
        
        ## Array to collect velocity data from both encoders
        self.vArray = [arr.array('f',[]), arr.array('f',[])]
        
        ## Array to collect time data from both motor/encoder combos
        #self.tArray = [[],[]]
        
        ## Array to collect position data from both encoders
        #self.pArray = [[],[]]
        
        ## Array to collect velocity data from both encoders
        #self.vArray = [[],[]]
        
        
        
    def run(self):
        '''@brief         Method that creates the user interface and FSM that interprets the user's character inputs.
           
           
        '''
        if(utime.ticks_diff(utime.ticks_ms(), self.nexttime) >= 0):
            self.nexttime += self.period
            if(self.state == S0_INIT):
                #Execute code for state 1
                print('--------------------------------\n'
                      '     USER COMMAND INTERFACE     \n'
                      '--------------------------------\n'
                      'z: Set encoder 1 position to zero \n'
                      'Z: Set encoder 2 position to zero \n'
                      'p: Print encoder 1 position to PuTTY \n'
                      'P: Print encoder 2 position to PuTTY \n'
                      'd: Print encoder 1 delta to puTTY \n'
                      'D: Print encoder 2 delta to puTTY \n'
                      'm: Set duty cycle for motor 1\n'
                      'M: Set duty cycle for motor 2\n'
                      'c: Clear fault condition triggerd by DRV8847\n'
                      'g: Collect encoder 1 data for 30 seconds, then print to puTTY\n'
                      'G: Collect encoder G data for 30 seconds, then print to puTTY\n'
                      's: End data collection prematurely\n'
                      'ESC: Display user interface controls\n'
                      '--------------------------------')
        
                self.transition_to(S1_WAIT_FOR_INPUT) #Transition to state 1
            
            elif(self.state == S1_WAIT_FOR_INPUT):
        
                keyCommand = self.read()
                
                self.write(keyCommand[0],self.Share1)

                self.write(keyCommand[0] + 32, self.Share2)
             
        

    
    
    def write(self,keyCommand,Shares):
        '''@brief              Method that interprets the user input via keyCommand and subsequently determines whether to display collected data, collect data, display the encoder delta, or
                                display the encoder position, velocity, or collect and display data. 
           @param              keyCommand ASCII value that corresponds to the character inputted by the user.
           @param              Shares Tuple value that contains the most recent encoder position and encoder delta values
           
        '''
        
        pos = Shares.read(POS)
        vel = Shares.read(VEL)
        en = Shares.read(EN) - 1
        duty = Shares.read(DUTY)
        
        
        if keyCommand == b'\x1b'[0]:
            self.transition_to(S0_INIT)
        
        elif keyCommand == b'z'[0]:
            Shares.write(ZERO, True)
            print(str(Shares.read(ZERO)))
            print('Zeroing encoder ' + str(en+1) + ' position')
            
        elif keyCommand == b'd'[0]:
            print('Motor ' +str(en+1) + ' Velocity [rad/s]:' + str(vel))
            
        elif keyCommand == b'p'[0]:
            print('Motor ' +str(en+1) + ' Position [rad]: ' + str(pos))
                
        elif keyCommand == b'g'[0] and not self.displayp[en]:
            print('Collecting ' + str(en+1) + ' data...')
            self.displayp[en] = True
            self.to = utime.ticks_ms()
            
        elif keyCommand == b'm'[0]:
            
            self.initDuty[en] = True
            self.zeroshare[en] = 0
            
        elif keyCommand == b'c'[0]:
            Shares.write(NO_FAULT, True)
            print('Motor ' +str(en+1) + ' fault disabled')
        
        if(self.displayp[en]):
            self.recordData(en, pos, vel, keyCommand == b's'[0])
            
        if(self.initDuty[en]):
            returnDuty = self.requestDuty(en, keyCommand, duty)
            if(returnDuty != duty):
                Shares.write(DUTY, returnDuty)
           
       # if (self.displayp == True and (keyCommand == b's' or utime.ticks_diff(utime.ticks_add(self.to, self.DisplayPositionTime), current_time) <= 0)):
        #    print('Time [sec], Position [ticks]')
         #   for n in range(len(self.tlist)):
          #      print('{:}, {:}'.format(self.tlist[n]/1000,self.plist[n]))
           # self.displayp = False
            #self.tlist = []
            #self.plist = []
            
       # elif(self.displayp):
       #     self.tlist.append(utime.ticks_diff(current_time, self.to))
       #     self.plist.append(pos)
        
      
    def read(self):
        '''@brief     Method that directly reads the VCP's character input and returns that character input to the run() method, or displays the user interface again.
           @return    Character inputted by the user via puTTY.
        '''
        
        #keyCommand = b''  #defining keyCommand so error is not thrown
        
        if(CommReader.any()):
            
            keyCommand = CommReader.read(1)
            
            CommReader.read()
             
            
            return keyCommand
            
        return b' '
          
    def recordData(self, e_n, POS, VEL, term):
        '''@brief               Transitions the FSM to a new state.
           @param e_n The motor ID, minus one.
           @param POS The shared motor position.
           @param VEL The shared motor velocity.
           @param term Boolean that turns true when the "s" key is pressed, to prematurely terminate data collection.
        '''
        
        
        current_time = utime.ticks_ms()
        
        if term or utime.ticks_diff(utime.ticks_add(self.to, self.DisplayPositionTime), current_time) <= 0:
           print('Motor ' + str(e_n+1) + ' Time [sec], Position [rad], Velocity [rad/s]')
           for n in range(len(self.tArray[e_n])):
                print('{:}, {:}, {:}'.format(self.tArray[e_n][n]/1000,self.pArray[e_n][n],self.vArray[e_n][n]))
           self.displayp[e_n] = False
           self.tArray[e_n] = []
           self.pArray[e_n] = []
           self.vArray[e_n] = []
           
        else:
           self.tArray[e_n].append(utime.ticks_diff(current_time, self.to))
           self.pArray[e_n].append(POS)
           self.vArray[e_n].append(VEL)
           
    def requestDuty(self,e_n,keyCommand,duty):
        '''@brief               Transitions the FSM to a new state.
           @param e_n  The motor ID, minus one.
           @param keyCommand Character input from the VCP.
           @param duty The initial duty cycle in the shared data.
           @return self.zeroshare[e_n] Float that represents the duty cycle input by the user
           @return duty The current motor duty cycle.
        '''
        keyCommand = keyCommand - e_n*32
        
        if (keyCommand>=b'0'[0] and keyCommand<=b'9'[0]):
            new = keyCommand - 48
            self.zeroshare[e_n] = self.zeroshare[e_n]*10 + new
        elif keyCommand == 13:
            self.initDuty[e_n] = False
            return self.zeroshare[e_n]
        elif keyCommand == 45:
            self.zeroshare[e_n] = -self.zeroshare[e_n]
            
        if(keyCommand != 32):
            print('\033c', end='')
            print('Input Motor ' + str(e_n+1) + ' PWM: ' + str(self.zeroshare[e_n]))
            
        return duty
    
    def transition_to(self, new_state):
        '''@brief               Transitions the FSM to a new state
           @param new_state     The next state in the FSM to transition to
        '''
        self.state = new_state
            
