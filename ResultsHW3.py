# -*- coding: utf-8 -*-
'''@file ResultsHW3.py

@page hw_3_page Simulation Plots

Here is a plot of the state variables for the open loop model with an initial position at the center of the ball-balancing platform:
                \image html open_loop_static.PNG "Figure 1. Open-loop simulated state variables plotted versus time, with initial conditions at the center of the ball-balancing platform."
Figure 1 reveals that the open-loop controlled system behaves staticly when the ball is placed at the center of the platform. Since open-loop control does not input any force on the system, placing
the ball at the center of the platform should not change the platform's position. The center of the platform is the equilibrum position, and thus placing the ball at the 
equilibrium position will not cause the system to leave the equilibrium state. <br>

Here is a plot of the state variables for the open loop model with an initial position 5 cm off-center on the ball-balancing platform:
                \image html open_loop_dynamic.PNG "Figure 2. Open-loop simulated state variables plotted versus time, with initial conditions 5 cm off-center on the ball-balancing platform."
Figure 2 displays a plot of the system state variables when the ball is placed 5 cm off the center of the x-axis on the ball and plate platform. As previously
stated, open-loop control does not provide any input to the system. If we inspect the subplot of position (state variable x) versus time, we see that the ball's position
trends up and away from equilibrium. This trend correlates to the ball weighing down on the platform and exerting a moment, but since the platform pushrods do not impart
any force, the ball simply rolls off the platform and away from the equilibrium position. <br>

Here is a plot of the state variables for the closed loop model with an initial position 5 cm off-center on the ball-balancing platform:
                \image html closed_loop_dynamic.PNG "Figure 3. Closed-loop simulated state variables plotted versus time, with initial conditions 5 cm off-center on the ball-balancing platform."
Figure 3 reveals the closed-loop control response. In closed-loop control, the platform pushrods DO exert a force on the plate platform, which resists the weight
of the ball that pushes the platform away from equilibrium. In Figure 3 the ball is again placed 5 cm off the platform's center. However, the pushrod now exerts a
counteracting moment on the plate which tends to push the ball back towards equilibrium. This trend towards equilibrium may be observed most easily in the position
versus time plot, which shows that the ball's position gradually trends toward the zero position on the x-axis. <br>
'''